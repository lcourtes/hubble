Hubble
=======

Hubble is a small project aiming to simulate scheduling algorithms of
build tasks on the Hydra continuous build system using SimGrid.  This
work is carried out in the Cepage Team-Project of INRIA Bordeaux
Sud-Ouest.

Hydra itself is based on the Nix build and deployment tool, both of
which are free software developed at the Technical University of Delft
(Netherlands).  Hubble does not depend on these tools; it merely
simulates them, using data collected using them.

More info on Hydra and Nix at <http://nixos.org/hydra/>.


Licensing Terms
----------------

Hubble is distributed under the terms of the GNU General Public License,
version 3.  See the file `COPYING' for details.

Requirements
-------------

  - GNU Guile 2.0.x, the Scheme implementation needed to run Hubble.
    http://gnu.org/software/guile/

  - SimGrid 3.5 (3.4.1 will *not* work), the grid simulator used by Hubble.
    http://simgrid.gforge.inria.fr/

Using
------

  $ guile -L . hubble.scm
