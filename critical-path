#!/bin/sh
# -*- mode: scheme; coding: utf-8; -*-
main='(@ (critical-path) critical-path)'
exec ${GUILE-guile} -L "$PWD/modules" -l "$0"    \
         -c "(apply $main (cdr (command-line)))" "$@"
!#
;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010, 2011 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (critical-path)
  #:use-module (hubble dag)
  #:use-module ((hubble scheduling) #:renamer (symbol-prefix-proc 'h:))
  #:use-module (hubble utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-37)
  #:export (critical-path))


;;;
;;; Entry point.
;;;

(define %defaults
  `((cores . 1)))

(define %options
    ;; Command-line options.
  (let ((display-and-exit-proc (lambda (fmt . args)
                                 (lambda _
                                   (apply format #t fmt args)
                                   (exit 0)))))
    (list (option '(#\V "version") #f #f
                  (display-and-exit-proc "hubble 0.0"))
          (option '(#\? "help") #f #f
                  (display-and-exit-proc
                   "Usage: critical-path [OPTIONS] FILE
Display the critical path for the DAG stored at FILE, a `.dag' XML file.

  -c, --cores=N         show the critical path when building on N cores

  -?, --help            give this help message
  -V, --version         print program version

Report bugs to <ludovic.courtes@inria.fr>.~%"))

          (option '(#\c "cores") #t #f
                  (lambda (opt name arg result)
                    (alist-cons 'cores (string->number arg) result))))))


(define (critical-path . args)
  "Display the critical path of the Nixpkgs DAG."

  (define opts
    (begin
      (setlocale LC_ALL "")
      (args-fold args %options
                 (lambda (opt name arg result)
                   (format (current-error-port) "~A: invalid option"
                           name)
                   (exit 1))
                 (lambda (arg result)
                   (alist-cons 'dag-file arg result))
                 %defaults)))

  (let ((dag-file (assoc-ref opts 'dag-file)))
    (if (not dag-file)
        (begin
          (format (current-error-port) "No DAG specified.~%")
          (exit 1))
        (let* ((dag   (load-dag dag-file))
               (cores (assoc-ref opts 'cores))
               (path  (phase "computing critical path"
                             (h:critical-path dag cores))))
          (for-each (lambda (n)
                      (format #t "~a\t~a~%"
                              (or (node-output-path n) (node-derivation-path n))
                              (if (node-duration n)
                                  (/ (node-duration n)
                                     (h:measured-node-speedup* n cores)
                                     1.0)
                                  "n/a")))
                    path)

          (format #t "total\t~a~%"
                  (fold (lambda (n t)
                          (+ t (if (node-duration n)
                                   (/ (node-duration n)
                                      (h:measured-node-speedup* n cores)
                                      1.0)
                                   0)))
                        0
                        path))
          #t))))
