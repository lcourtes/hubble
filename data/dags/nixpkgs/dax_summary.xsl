<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
	  number of nodes: <xsl:value-of select="count(//adag/job)"/>
	  volume of communications (in Bytes): <xsl:value-of select="sum(//adag/job/uses[@link='input']/@size)"/>
	  volume of computation (in GFlop): <xsl:value-of select="sum(//adag/job/@runtime)*4.2"/>
	  volume of computation (in s on 91.5GFlops machines): <xsl:value-of select="sum(//adag/job/@runtime)*0.0459"/>
	  number of dependencies: <xsl:value-of select="count(//adag/job/uses[@link='input']/@size)"/>
	  number of size 0 dependencies: <xsl:value-of select="count(//adag/job/uses[@size=0])"/>
  </xsl:template>
</xsl:stylesheet> 