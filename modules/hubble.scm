;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; -*- coding: utf-8 -*-
;;; Copyright (C) 2010 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (hubble)
  #:use-module (hubble dag)
  #:use-module (hubble utils)
  #:use-module (hubble output)
  #:use-module (hubble scheduling)
  #:use-module (simgrid)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-37)
  #:export (hubble))



;;;
;;; Main program.
;;;

(define %task-selectors
  `((first-task  . ,(lambda _ select-first-task))
    (random-task . ,(lambda _ select-random-task))
    (heft        . ,make-heft-task-selector)))

(define %workstation-selectors
  `((first-workstation  . ,select-first-workstation)
    (random-workstation . ,select-random-workstation)
    (heft               . ,select-workstation/heft)
    (m-heft             . ,select-workstation/m-heft)
    (many-cores         . ,select-many-cores)
    (2-cores         . ,select-2-cores)
    (4-cores         . ,select-4-cores)
    (8-cores         . ,select-8-cores)))

(define %defaults
  ;; Default option values.
  `((output        . "hubble.trace")
    (platform      . "data/platforms/default.xml")
    (task-selector . ,(cdar %task-selectors))
    (workstation-selector . ,(cdar %workstation-selectors))
    (simdag-options)))

(define %options
  ;; Command-line options.
  (let ((display-and-exit-proc (lambda (fmt . args)
                                 (lambda _
                                   (apply format #t (string-append fmt "~%")
                                          args)
                                   (exit 0)))))
    (list (option '(#\V "version") #f #f
                  (display-and-exit-proc "hubble 0.0"))
          (option '(#\? "help") #f #f
                  (display-and-exit-proc
                   "Usage: hubble [OPTIONS] DAG
Simulate the scheduling of the build tasks in DAG on a cluster.

  -o, --output=FILE     store the Pajé trace in FILE (default: `~A')
  -s, --simdag-options=OPTS
                        pass options OPTS to `SD_init', SimDAG's
                        initialization routine

  -p, --platform=FILE   use FILE as the XML SimGrid platform description file
                        (default: `~A')
  -t, --task-selector=ALGO
                        use ALGO to select a task to schedule among the set
                        of ready tasks (default: ~A;
                        possible values: ~A)
  -w, --workstation-selector=ALGO
                        use ALGO to select from a set of idle workstation
                        where the next task will be executed (default: ~A;
                        possible values: ~A)

  -?, --help            give this help message
  -V, --version         print program version"
                   (and=> (assq 'output %defaults) cdr)
                   (and=> (assq 'platform %defaults) cdr)
                   (caar %task-selectors)
                   (string-join (map symbol->string
                                     (map car %task-selectors))
                                ", ")
                   (caar %workstation-selectors)
                   (string-join (map symbol->string
                                     (map car %workstation-selectors))
                                ", ")))

          (option '(#\o "output") #t #f
                  (lambda (opt name arg result)
                    (alist-cons 'output arg
                                (alist-delete 'output result))))
          (option '(#\s "simdag-options") #t #f
                  (lambda (opt name arg result)
                    (alist-cons 'simdag-options (string-tokenize arg)
                                (alist-delete 'simdag-options result))))
          (option '(#\p "platform") #t #f
                  (lambda (opt name arg result)
                    (alist-cons 'platform arg
                                (alist-delete 'platform result))))
          (option '(#\t "task-selector") #t #f
                  (lambda (opt name arg result)
                    (let ((arg (string->symbol arg)))
                      (alist-cons 'task-selector
                                  (or (and=> (assq arg %task-selectors)
                                             cdr)
                                      (begin
                                        (format (current-error-port)
                                                "~A: unknown task selection algorithm"
                                                arg)
                                        (exit 1)))
                                  (alist-delete 'task-selector result)))))
          (option '(#\w "workstation-selector") #t #f
                  (lambda (opt name arg result)
                    (let ((arg (string->symbol arg)))
                      (alist-cons 'workstation-selector
                                  (or (and=> (assq arg %workstation-selectors)
                                             cdr)
                                      (begin
                                        (format (current-error-port)
                                                "~A: unknown workstation selection algorithm"
                                                arg)
                                        (exit 1)))
                                  (alist-delete 'workstation-selector result))))))))


(define (hubble . args)
  (define opts
    (begin
      (setlocale LC_ALL "")

      (args-fold args %options
                 (lambda (opt name arg result)
                   (format (current-error-port) "~A: invalid option"
                           name)
                   (exit 1))
                 (lambda (arg result)
                   (if (assq 'dag result)
                       (begin
                         (format (current-error-port)
                                 "~A: extraneous argument: DAG file already specified"
                                 args)
                         (exit 1))
                       (alist-cons 'dag arg result)))
                 %defaults)))

  (define dag
    ;; The build tasks.
    (let ((file (assoc-ref opts 'dag)))
      (if file
          (load-dag file)
          (begin
            (format (current-error-port) "Please specify a DAG file.~%")
            (exit 1)))))

  (define make-task-selector
    (and=> (assq 'task-selector opts) cdr))

  (define select-workstation
    (and=> (assq 'workstation-selector opts) cdr))

  (display-dag-stats dag (current-output-port))

  (phase "initializing SimGrid"
         (simdag-init! (and=> (assq 'simdag-options opts) cdr)))

  (let ((file (and=> (assq 'platform opts) cdr)))
    (phase "creating SimGrid environment with platform file `~a'" file
           (make-simdag-environment file)))

  (let* ((workstations (sort (simdag-workstations)
                             (lambda (w1 w2)
                               (let ((n1 (simdag-workstation-name w1))
                                     (n2 (simdag-workstation-name w2)))
                                 (string<? n1 n2)))))
         (tasks
          (dynamic-scheduling dag workstations
                              (make-task-selector dag workstations)
                              select-workstation)))
    (display-simulation-stats dag tasks workstations)

    (call-with-output-file (and=> (assq 'output opts) cdr)
      (lambda (port)
        (setvbuf port _IOFBF 32768)
        (phase "producing Pajé output"
               (simdag->pajé tasks workstations port)))))

  (simdag-exit!))
