;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010, 2011, 2012 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (hubble dag)
  #:use-module (hubble utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-13)
  #:use-module (srfi srfi-26)
  #:use-module ((sxml simple) #:select (xml->sxml sxml->xml))
  #:autoload   (hubble scheduling)     (seconds->flops) ; FIXME: circ dep
  #:use-module (sxml match)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 i18n)
  #:use-module (ice-9 format)
  #:use-module (ice-9 futures)

  ;; For the DAX auto-calibration.
  #:autoload   (simgrid)         (simdag-init!)
  #:autoload   (hubble platform) (make-sxml-platform)

  #:export (make-node
            node?
            node-output-path
            node-derivation-path
            node-dependencies
            node-recursive-dependencies
            node-run-time-dependencies
            node-run-time-dependencies*
            node-recursive-run-time-dependencies
            node-prerequisites
            node-referrers
            node-size
            node-duration
            node-fixed-output?
            node-speedup

            run-time/compile-time-dependency-size-ratio

            make-dag
            dag?
            dag-nodes
            dag-sinks
            dag-sources
            dag=?

            dag-fold/breadth-first
            dag-fold/depth-first
            dag-topological-sort
            zero-fixed-output-nodes

            sxml->dag
            dag->sxml
            dag->sxml/dax
            load-dag
            dag->dot

            parse-size/duration
            display-dag-stats

            load-nix-dag
            build-events->dag))


;;;
;;; DAGs of tasks.
;;;

(define-record-type node
  ;; Nodes, i.e., Nix derivations and their realization.
  (%make-node out-path drv-path deps refs rtdeps size duration fixed-output?
              speedup recursive-rt-deps)
  node?
  (out-path       node-output-path)
  (drv-path       node-derivation-path)
  (deps           node-dependencies)
  (refs           node-referrers)
  (rtdeps         node-run-time-dependencies) ;; a promise, which may be #f
  (size           node-size)  ;; size in bytes of the build output
  (duration       node-duration)
  (fixed-output?  node-fixed-output?)
  (speedup        node-speedup) ;; ((NB-CORES . SPEEDUP) ...)

  ;; Promises used internally for memoization purposes.
  (recursive-rt-deps %node-recursive-run-time-dependencies))

(define* (make-node out-path drv-path deps refs rtdeps size duration
                    fixed-output? #:optional (speedup '()))
  "Create a DAG node."

  (define (recursive-run-time-deps n)
    ;; Return the recursive run-time dependencies of N.  Since this is a
    ;; costly operation, we memoize it, via the `recursive-rt-deps' promise.
    (let loop ((nodes   (node-run-time-dependencies* n))
               (result  '())
               (visited vlist-null))
      (if (null? nodes)
          result
          (let ((n (car nodes)))
            (if (vhash-assq n visited)
                (loop (cdr nodes) result visited)
                (loop (append (cons n (node-run-time-dependencies* n)) nodes)
                      (cons n result)
                      (vhash-consq n #t visited)))))))

  (letrec ((n (%make-node out-path drv-path deps refs rtdeps size
                          duration fixed-output? speedup
                          (delay (recursive-run-time-deps n)))))
    n))

(set-record-type-printer! node
                          (lambda (node port)
                            (format port "#<node ~x ~s ~s>"
                                    (object-address node)
                                    (basename (node-output-path node))
                                    (and=> (node-derivation-path node)
                                           basename))))

(define-record-type dag
  ;; Directed acyclic graph of nodes.
  (%make-dag nodes sources sinks)
  dag?
  (nodes   dag-nodes)
  (sinks   dag-sinks)
  (sources dag-sources))

(define (make-dag nodes sources sinks)
  "Return a new directed acyclic graph (DAG) consisting of NODES; its sources
are SOURCES (a subset of NODES) and its sinks are SINKS (another subset of
NODES).  Check the consistency of the DAG by traversing in different ways."
  (define (subset? sub super)
    (every (cut memq <> super) sub))

  (let* ((dag (%make-dag nodes sources sinks))
         (t1  (dag-fold/breadth-first cons '()
                                      (dag-sinks dag)))
         (t2  (dag-fold/depth-first cons '()
                                    (dag-sources dag)
                                    node-referrers)))
    ;; Make sure we see the same set of nodes by traversing DAG in both
    ;; directions.
    (if (every (cut lset= eq? <> nodes) (list t1 t2))
        (if (every (lambda (n)
                     ;; Make sure the run-time dependencies of each node is a
                     ;; subset of its compile-time dependencies.
                     (let ((rt-deps (force (node-run-time-dependencies n))))
                       (or (not rt-deps)
                           (subset? rt-deps (node-prerequisites n)))))
                   t1)
            dag
            (begin
              (format (current-error-port)
                      "some nodes have rt-deps not a subset of deps~%")
              (throw 'dag-consistency-error nodes)))
        (let ((d1 (lset-difference eq? nodes t1))
              (d2 (lset-difference eq? nodes t2)))
          (throw 'dag-consistency-error d1 d2)))))

(define* (dag-fold/breadth-first proc init nodes
                                 #:optional (traverse node-dependencies))
  ;; Perform a breadth-first traversal of the DAG rooted at NODES, a list of
  ;; `node' objects.  The convention is the same as for SRFI-1 `fold'.
  ;; TRAVERSE can be `node-dependencies' or `node-referrers'.
  (let loop ((nodes   nodes)
             (result  init)
             (visited vlist-null))
    (if (null? nodes)
        result
        (let* ((result  (fold proc result nodes))
               (visited (fold (cut vhash-consq <> #t <>) visited nodes))
               (rels    (fold (lambda (n s)
                                (let ((r (force (traverse n))))
                                  ;; Drop nodes already visited or already
                                  ;; dragged by an element of NODES.
                                  (append (filter (lambda (n)
                                                    (and (not
                                                          (vhash-assq n
                                                                      visited))
                                                         (not (memq n s))))
                                                  r)
                                          s)))
                              '()
                              nodes)))
          (loop rels result visited)))))

(define* (dag-fold/depth-first proc init nodes
                               #:optional (traverse node-dependencies))
  ;; Perform a depth-first traversal of the DAG rooted at NODES, a list of
  ;; `node' objects.  The convention is the same as for SRFI-1 `fold'.
  ;; TRAVERSE can be `node-dependencies' or `node-referrers'.
  (let loop ((nodes   nodes)
             (result  init)
             (visited vlist-null))
    (if (null? nodes)
        result
        (let ((node    (car nodes))
              (nodes   (cdr nodes)))
          (if (vhash-assq node visited)
              (loop nodes result visited)
              (loop (append (force (traverse node)) nodes)
                    (cons node result)
                    (vhash-consq node #t visited)))))))

(define (node-recursive-dependencies n)
  "Return the dependencies of N, recursively."
  (dag-fold/breadth-first cons '()
                          (force (node-dependencies n))
                          node-dependencies))

(define (node-run-time-dependencies* n)
  "Return the run-time dependencies of NODE or, if this information isn't
available, its compile-time dependencies."
  (define %flat-extensions
    '("gz" "bz2" "xz" "lzma" "zip" "tar"
      "jpg" "png" "gif" "pdf" "ps"))

  (define (file-extension file)
    (and=> (string-rindex file #\.)
           (lambda (dot)
             (substring file (+ 1 dot) (string-length file)))))

  (let ((d (force (node-run-time-dependencies n))))
    (cond (d d)
          ((member (file-extension (node-output-path n))
                   %flat-extensions)
           ;; XXX: Hack to work around missing run-time dependency info.
           '())
          (else (force (node-dependencies n))))))

(define (node-recursive-run-time-dependencies n)
  "Return the run-time dependencies of N, recursively, N excluded.  For nodes
lacking run-time dependency information, use the direct compile-time
dependencies instead of the recursive run-time dependencies."
  ;; See `make-node' for the actual computation.
  (force (%node-recursive-run-time-dependencies n)))

(define (node-prerequisites n)
  "Return the list of build prerequisites of N, i.e., all its direct
compile-time dependencies, and all their recursive run-time dependencies."
  (let ((deps (force (node-dependencies n))))
    (delete-duplicates
     (append deps
             (append-map node-recursive-run-time-dependencies
                         deps))
     eq?)))

(define (run-time/compile-time-dependency-size-ratio n)
  "Return the ratio of the size of size of N's direct compile time
dependencies and their recursive run-time dependencies, to the the size of
N's direct compile-time dependencies."
  (define (node-size* n)
    (or (node-size n) 0))

  (let* ((deps      (force (node-dependencies n)))
         (full-deps (node-prerequisites n))
         (full      (reduce + 0.0 (map node-size* full-deps)))
         (direct    (reduce + 0.0 (map node-size* deps))))
    (if (zero? direct)
        1.0
        (/ full direct 1.0))))

(define (dag-topological-sort dag)
  ;; Return a topologically sorted list of nodes from DAG.  This is an
  ;; implementation of the sorting algorithm by A. B. Kahn, “Topological
  ;; sorting of large networks”, 1962, as described at
  ;; <https://secure.wikimedia.org/wikipedia/en/wiki/Topological_sorting>.

  (define (unvisited-dependencies n visited)
    ;; Return the list of unvisited "incoming edges" of N, i.e., unvisited
    ;; dependency edges of N.
    (let ((visited (vhash-foldq* cons '() n visited)))
      (filter (lambda (d)
                (not (memq d visited)))
              (force (node-dependencies n)))))

  (let loop ((sources       (dag-sources dag))
             (result        '())
             (visited-edges vlist-null))
    (if (null? sources)
        (reverse result)
        (let* ((node          (car sources))
               (sources       (cdr sources))
               (visited-edges (fold (cut vhash-consq <> node <>)
                                    visited-edges
                                    (force (node-referrers node)))))
          (loop (fold (lambda (n r)
                        (if (null? (unvisited-dependencies n visited-edges))
                            (cons n r)
                            r))
                      sources
                      (force (node-referrers node)))
                (cons node result)
                visited-edges)))))

(define* (dag=? d1 d2 #:optional (normalize? #t))
  "Return #t if DAGs D1 and D2 are equal.  If NORMALIZE? is true, then
dependencies/referrers are sorted before comparing nodes, such that
comparison is insensitive to the order of dependencies/referrers."
  ;; FIXME: Compare run-time dependencies.

  (define (node-speedup* n)
    ;; Round the speedup to avoid spurious differences when comparing inexact
    ;; numbers.
    (map (match-lambda
          ((cores . speedup)
           (cons cores (round speedup))))
         (node-speedup n)))

  (define %fields
    (list node-output-path node-derivation-path
          node-size node-duration node-fixed-output?
          node-speedup*))

  (define (node<? n1 n2)
    (string<? (node-output-path n1)
              (node-output-path n2)))

  (let loop ((nodes1  (dag-sources d1))
             (nodes2  (dag-sources d2))
             (visited vlist-null))
    (if (null? nodes1)
        (null? nodes2)
        (let ((n1      (car nodes1))
              (n2      (car nodes2))
              (nodes1  (cdr nodes1))
              (nodes2  (cdr nodes2)))
          (if (vhash-assq n1 visited)
              (loop nodes1 nodes2 visited)
              (let ((r1 (let ((x (force (node-referrers n1))))
                          (if normalize?
                              (sort x node<?)
                              x)))
                    (r2 (let ((x (force (node-referrers n2))))
                          (if normalize?
                              (sort x node<?)
                              x))))
                (and (every (lambda (f)
                              (equal? (f n1) (f n2)))
                            %fields)
                     (loop (append r1 nodes1)
                           (append r2 nodes2)
                           (vhash-consq n1 #t visited)))))))))

(define (zero-fixed-output-nodes dag)
  "Return a new DAG based on DAG where fixed-output nodes have been changed
to have a duration equal to zero and to have no dependencies."
  ;; XXX: The new DAG refers to the old one until all its promises have been
  ;; forced.
  (define (node-dependencies* n)
    (if (node-fixed-output? n)
        '()
        (force (node-dependencies n))))

  (define (node-referrers* n)
    (remove node-fixed-output? (force (node-referrers n))))

  (letrec ((old->new
            (lambda (n)
              (and=> (vhash-assq n new) cdr)))
           (new (fold (lambda (n r)
                        (let* ((d  (if (node-fixed-output? n)
                                       0
                                       (node-duration n)))
                               (n* (make-node (node-output-path n)
                                              (node-derivation-path n)
                                              (delay
                                                (map old->new
                                                     (node-dependencies* n)))
                                              (delay
                                                (map old->new
                                                     (node-referrers* n)))
                                              (delay
                                                (let ((d
                                                       (force
                                                        (node-run-time-dependencies
                                                         n))))
                                                  (if d
                                                      (map old->new d)
                                                      d)))
                                              (node-size n)
                                              d
                                              (node-fixed-output? n)
                                              (node-speedup n))))
                          (vhash-consq n n* r)))
                      vlist-null
                      (dag-nodes dag))))

    (let ((nodes (map old->new (dag-nodes dag))))
      (make-dag nodes
                (filter (compose null? force node-dependencies) nodes)
                (filter (compose null? force node-referrers) nodes)))))

(define hydra-seconds->dax-seconds
  (let ((flops/second (delay (dax-flops/second-ratio))))
    (lambda (s)
      "Convert hydra.nixos.org execution times (in seconds) to the execution
time of the same task on SimDAG, given the built-in flops/second ratio of
`SD_daxload'."
      (/ (seconds->flops s)
         (force flops/second)))))

(define* (dag->sxml/dax dag
                        #:key (convert-duration hydra-seconds->dax-seconds)
                              (name "Daxy the DAG"))
  "Return the DAX (SXML) representation of DAG.  The DAX format is documented
at `https://confluence.pegasus.isi.edu/display/pegasus/WorkflowGenerator' and
can be used by SimDAG via `SD_daxload'."
  (define output-paths/nodes
    ;; All the nodes.  Keep only one node per output path.
    (fold-right (lambda (n result)
                  (if (vhash-assoc (node-output-path n) result)
                      result
                      (vhash-cons (node-output-path n) n result)))
                vlist-null
                (dag-topological-sort dag)))

  (define (canonicalize-node n)
    (cdr (vhash-assoc (node-output-path n) output-paths/nodes)))

  (define (node-prerequisites* n)
    ;; Filter out the prerequisites of fixed-output nodes.
    (if (node-fixed-output? n)
        '()
        (node-prerequisites n)))

  (define children
    (delete-duplicates
     (append-map (compose (cut map canonicalize-node <>)
                          node-prerequisites* canonicalize-node)
                 (dag-sinks dag))))

  (define referrers
    (let ((back-links (fold (lambda (n r)
                              (fold (cut vhash-consq <> n <>)
                                    r
                                    (node-prerequisites* n)))
                            vlist-null
                            (dag-nodes dag))))
      (lambda (n)
        (vhash-foldq* cons '() n back-links))))

  (define (job-id n)
    (number->string (object-address n) 32))

  (define (make-job n)
    (let ((deps (node-prerequisites* n)))
      `(job (@ (id       ,(job-id n))
               (name     ,(or (node-derivation-path n)
                              (node-output-path n)))
               (runtime  ,(number->string
                           (if (node-duration n)
                               (convert-duration (node-duration n))
                               0)))
               (version  "1.0"))

            (uses (@ (file ,(string-append (node-output-path n)
                                           "/the-output"))
                     (size ,(number->string (or (node-size n) 0)))
                     (transfer "true")
                     (link "output")
                     (optional "false")
                     (type "data")))
            "\n"
            ,@(if (and (null? deps)
                       (not (null? (cdr (dag-sources dag)))))
                  `((uses (@ (file "source")      ; fake input to please SimDAG
                             (size "0")
                             (transfer "true")
                             (link "input")
                             (optional "false")
                             (type "data"))))
                  (append-map (lambda (d)
                                `((uses (@ (file ,(string-append
                                                   (node-output-path d)
                                                   "/the-output"))
                                           (size ,(number->string
                                                   (or (node-size d) 0)))
                                           (transfer "true")
                                           (link "input")
                                           (optional "false")
                                           (type "data")))
                                  "\n"))
                              deps)))))

  (define (make-child n)
    (let ((refs (referrers n)))
      (if (null? refs)
          '()
          `("\n"
            (child (@ (ref ,(job-id n)))
                   ,@(map (lambda (p)
                            `(parent (@ (ref ,(job-id p)))))
                          refs))
            "\n"))))

  (let ((nodes (vhash-fold (lambda (out-path n nodes)
                             (cons n nodes))
                           '()
                           output-paths/nodes)))
    `(*TOP*
      (*PI* xml "version='1.0' encoding='utf-8'")
      "\n\n"
      (adag (@ (xsi:schemaLocation
                "http://pegasus.isi.edu/schema/DAX
http://pegasus.isi.edu/schema/dax-2.1.xsd")
               (version "2.1")
               (count "1")
               (index "0")
               (name ,name)
               (jobCount ,(number->string (length nodes)))
               (fileCount "0")
               (childCount ,(number->string (length children))))

            ,@(map make-job nodes)

            ;; XXX: This isn't needed for SimGrid 3.4.1.
            ;; ,@(append-map make-child children)
            ))))

(define (dag->sxml dag)
  "Return an SXML representation of DAG."
  (define (make-edge d)
    `(edge (@ (src ,(or (node-derivation-path d)
                        (node-output-path d))))))

  (define (make-speedup s)
    `(speedup (@ (cores ,(number->string (car s))))
              ,(number->string (cdr s))))

  (define (node->sxml n)
    `(node (@ (out ,(node-output-path n))
              ,@(if (node-derivation-path n)
                    `((drv ,(node-derivation-path n)))
                    '())
              ,@(if (node-size n)
                    `((size ,(number->string (node-size n))))
                    '())
              ,@(if (node-duration n)
                    `((duration ,(number->string (node-duration n))))
                    '())
              ,@(if (node-fixed-output? n)
                    '((fixed "yes"))
                    '()))

           ;; compile-time dependencies (mandatory)
           (dependencies
            ,@(map make-edge (force (node-dependencies n))))

           ;; run-time dependencies (optional)
           ,@(if (not (force (node-run-time-dependencies n)))
                 '()
                 `((run-time-dependencies
                    ,@(map make-edge
                           (force (node-run-time-dependencies n))))))

           ;; speedup (optional)
           ,@(if (null? (node-speedup n))
                 '()
                 `((parallelism
                    ,@(map make-speedup (node-speedup n)))))))

  `(*TOP*
    (*PI* xml "version='1.0' encoding='utf-8'")
    (dag
     ,@(reverse (dag-fold/breadth-first (lambda (n r)
                                          (cons (node->sxml n) r))
                                        '()
                                        (dag-sources dag)
                                        node-referrers)))))

(define (sxml->dag sxml)
  "Load SXML, the SXML representation of a DAG, as a DAG object."
  (define nodes
    (sxml-match sxml
      ;; XXX: sxml-match fails to match large bodies so we assume BODY is a
      ;; <dag> element.
      ((*TOP* (*PI* ,foo ...) ,body)
       (cdr body))))

  (define (maybe-rt-deps rest)
    ;; XXX: Function introduced to work around a bug with nested `sxml-match'
    ;; forms and ellipses.
    (match rest
      ((x r ...)
       (sxml-match x
         ((run-time-dependencies ,rtd ...)
          rtd)
         (,_ (maybe-rt-deps r))))
      (else #f)))

  (define (maybe-parallelism rest)
    (match rest
      ((x r ...)
       (sxml-match x
         ((parallelism ,n ...)
          (filter-map (lambda (n)
                        (sxml-match n
                          ((speedup (@ (cores ,cores)) ,speedup)
                           (cons (string->number cores)
                                 (string->number speedup)))
                          (,_ #f)))
                      n))
         (,_ (maybe-parallelism r))))
      (else #f)))

  (letrec ((nodes+edges
            (fold (lambda (node nodes+edges)
                    (sxml-match node
                      ((node (@ (out (,out #f))
                                (drv (,drv #f))
                                (size (,size #f))
                                (duration (,duration #f))
                                (fixed (,fixed? #f)))
                             (dependencies ,deps ...)
                             ,rest ...)
                       (let* ((name   (or drv out))
                              (rtdeps (maybe-rt-deps rest))
                              (speedup (maybe-parallelism rest))
                              (node (make-node out drv
                                               (delay
                                                 (find-deps name))
                                               (delay
                                                 (find-refs name))
                                               (delay
                                                 (if rtdeps
                                                     (find-rt-deps name)
                                                     #f))
                                               (if size
                                                   (string->number size)
                                                   #f)
                                               (if duration
                                                   (string->number duration)
                                                   #f)
                                               (and fixed?
                                                    (string-ci=? fixed?
                                                                 "yes"))
                                               (or speedup '()))))
                         (cons* (if drv
                                    (vhash-cons drv node (first nodes+edges))
                                    (first nodes+edges))
                                (vhash-cons out node (second nodes+edges))
                                (fold (lambda (e r)
                                        (sxml-match e
                                          ((edge (@ (src ,dep)))
                                           (cons*
                                            (vhash-cons dep name (first r))
                                            (vhash-cons name dep (second r))
                                            (cddr r)))))
                                      (if (pair? rtdeps)
                                          (fold (lambda (e r)
                                                  (sxml-match e
                                                    ((edge (@ (src ,dep)))
                                                     (list (first r) (second r)
                                                           (vhash-cons name dep
                                                                       (third r))))))
                                                (cddr nodes+edges)
                                                rtdeps)
                                          (cddr nodes+edges))
                                      deps))))))
                  `(,vlist-null  ;; 1. drv path -> node
                    ,vlist-null  ;; 2. out path -> node
                    ,vlist-null  ;; 3. path     -> paths of deps
                    ,vlist-null  ;; 4. path     -> paths of refs
                    ,vlist-null) ;; 5. path     -> paths of run-time deps
                  nodes))

           (find-node (lambda (name)
                        (or (find-drv name) (find-out name))))
           (find-drv  (lambda (drv)
                        (and=> (vhash-assoc drv (first nodes+edges))
                               cdr)))
           (find-out  (lambda (out)
                        (and=> (vhash-assoc out (second nodes+edges))
                               cdr)))
           (find-refs (lambda (name)
                        (map find-node
                             (vhash-fold* cons '() name (third
                                                         nodes+edges)))))
           (find-deps (lambda (name)
                        (map find-node
                             (vhash-fold* cons '() name (fourth
                                                         nodes+edges)))))
           (find-rt-deps (lambda (name)
                           (map find-node
                                (vhash-fold* cons '() name (fifth nodes+edges))))))

    (let ((nodes (vhash-fold (lambda (path node result)
                               (cons node result))
                             '()
                             (second nodes+edges))))
      (make-dag nodes

                ;; sources (nodes with no dependencies)
                (filter (lambda (node)
                          (let ((path (or (node-derivation-path node)
                                          (node-output-path node))))
                            (not (vhash-assoc path (fourth nodes+edges)))))
                        nodes)

                ;; sinks (nodes not depended on)
                (filter (lambda (node)
                          (let ((path (or (node-derivation-path node)
                                          (node-output-path node))))
                            (not (vhash-assoc path (third nodes+edges)))))
                        nodes)))))

(define (load-dag file-or-port)
  "Convenience function to load a DAG from FILE-OR-PORT, an XML file or input
port to such a file."
  (define file-name
    (cond ((string? file-or-port)
           file-or-port)
          ((string? (port-filename file-or-port))
           (port-filename file-or-port))
          ((= 0 (fileno file-or-port))
           "<stdin>")
          (else file-or-port)))

  (let* ((s (phase "reading `~a'" file-name
                   (if (string? file-or-port)
                       (call-with-input-file file-or-port xml->sxml)
                       (xml->sxml file-or-port))))
         (d (phase "building DAG" (sxml->dag s))))
    (phase "zeroing fixed-output derivations"
           (zero-fixed-output-nodes d))))

(define (dag->dot dag port)
  "Write to PORT a GraphViz/Dot representation of DAG."
  (format port "digraph nixpkgs {~%")
  (for-each (lambda (n)
              (define name
                (basename (node-output-path n)))

              (format port "  \"~a\";~%" name)
              (for-each (lambda (d)
                          (format port "  \"~a\" -> \"~a\";~%"
                                  name (basename (node-output-path d))))
                        (force (node-dependencies n)))
              (match (force (node-run-time-dependencies n))
                ((rt-deps ...)
                 (for-each (lambda (d)
                             (format port "  \"~a\" -> \"~a\" [style=dotted];~%"
                                     name (basename (node-output-path d))))
                           rt-deps))))
            (dag-nodes dag))
  (format port "}~%"))


;;;
;;; Nix.
;;;

(define (nixpkgs->dag sxml size/duration
                      derivation/output-paths fixed-output-nodes
                      run-time-deps speedup)
  "Return a list of `node' records forming a DAG and representing the build
tasks described by SXML and SIZE/DURATION.  SXML and SIZE/DURATION are
matched by comparing output paths since comparing derivation paths would not
be reliable (different derivations can lead to the same output paths, due to
the fact that output paths are computed modulo fixed-output derivations.)
DERIVATION/OUTPUT-PATHS is a list of derivation/output path pairs.
FIXED-OUTPUT-NODES is a list of derivation or output paths of fixed-output
derivations.  RUN-TIME-DEPS is a list of pairs of output paths and their
dependencies.  SPEEDUP is a list of the form
`((PACKAGE . ((NB-CORES SPEEDUP (PHASES ...)) ...)) ...)', which tells the measured
speedup for each PACKAGE for each of `make -j NB-CORES'."
  (define body (cdaddr sxml)) ;; XXX: sxml-match fails to match large bodies

  (define derivation->output-path
    (let ((drv->out-path
           ;; Load the mapping alist in a vhash.
           (fold (lambda (drv+out result)
                   (vhash-cons (car drv+out) (cdr drv+out) result))
                 vlist-null
                 derivation/output-paths)))
      (lambda (drv-path)
        ;; Return the output path corresponding to DRV-PATH.
        (and=> (vhash-assoc drv-path drv->out-path) cdr))))

  (define output->derivation-path
    (let ((out->drv-path
           ;; Load the mapping alist in a vhash.
           (fold (lambda (drv+out result)
                   (vhash-cons (cdr drv+out) (car drv+out) result))
                 vlist-null
                 derivation/output-paths)))
      (lambda (out-path)
        ;; Return the output path corresponding to DRV-PATH.
        (and=> (vhash-assoc out-path out->drv-path) cdr))))

  (define (canonicalize-node-name path)
    (or (output->derivation-path path) path))

  (define (find-speedup out-path)
    (let ((short-path (strip-store-path out-path)))
      (match (find (lambda (p)
                     ;; Fuzzy-match the output path.  This is because the
                     ;; speedup of `package-X.Y' is likely to be valid,
                     ;; even if we are not building the exact same output
                     ;; path.
                     (string-suffix? short-path (car p)))
                   speedup)
        ((_ . cores/speedup+phases)
         (map (lambda (x)
                ;; Filter out build phase information.
                (match x
                  ((nb-cores speedup (_ ...))
                   (cons nb-cores speedup))))
              cores/speedup+phases))
        (_ '()))))

  (define fixed-output
    (fold (cut vhash-cons <> #t <>)
          vlist-null
          fixed-output-nodes))

  (let-values (((names edges back-edges)
                (apply values
                 (fold (lambda (node names+edges)
                         (let-values (((names edges back-edges)
                                       (apply values names+edges)))
                           (sxml-match node
                             ((edge (@ (src ,src) (dst ,dst)))
                              (list names
                                    (vhash-cons dst src edges)
                                    (vhash-cons src dst back-edges)))
                             ((node (@ (name ,name)))
                              (list (cons name names)
                                    edges
                                    back-edges))
                             (,else names+edges))))
                       `(() ,vlist-null ,vlist-null)
                       body))))
    (letrec ((find-node (lambda (path)
                          (and=> (vhash-assoc path nodes) cdr)))
             (nodes
              (fold (lambda (node result)
                      (let* ((drv?     (derivation-path? node))
                             (drv-path (and drv? node))
                             (out-path (if drv?
                                           (derivation->output-path node)
                                           node))
                             (deps
                              (delay
                                ;; Return the nodes pointed to by OUT-PATH.
                                (vhash-fold* (lambda (dep result)
                                               (cons (find-node dep) result))
                                             '()
                                             (or drv-path out-path)
                                             edges)))
                             (refs
                              (delay
                                ;; Return the nodes pointing to OUT-PATH.
                                (vhash-fold* (lambda (ref result)
                                               (cons (find-node ref) result))
                                             '()
                                             (or drv-path out-path)
                                             back-edges)))

                             (rtdeps
                              (delay
                                (and=> (assoc-ref run-time-deps out-path)
                                       (lambda (rtd)
                                         (let ((direct
                                                (append (force deps)
                                                        (append-map
                                                         node-recursive-run-time-dependencies
                                                         (force deps)))))
                                           (filter (lambda (d)
                                                     ;; Keep only direct deps.
                                                     (memq d direct))
                                                   (map find-node
                                                        (map
                                                         canonicalize-node-name
                                                         rtd))))))))

                             ;; INFO is built by matching OUT-PATH with the
                             ;; output paths in SIZE/DURATION.  When DRV? is
                             ;; #f, which is the case for `foo-1.2.tar.gz',
                             ;; `builder.sh', etc., we can't find any match,
                             ;; but that's OK.
                             (info (or (and=> (vhash-assoc out-path
                                                           size/duration)
                                              cdr)
                                       (and drv-path
                                            (and=> (vhash-assoc drv-path
                                                                size/duration)
                                                   cdr))
                                       (cons #f #f)))

                             (fixed-output?
                              (not (not (vhash-assoc out-path
                                                     fixed-output)))))

                        ;; There can be several drv paths mapped to one
                        ;; output path (e.g., because the same tarball is
                        ;; downloaded once with the boostrap environment and
                        ;; then with the final environment), but we always
                        ;; keep all of them.
                        (vhash-cons (or drv-path out-path)
                                    (make-node out-path drv-path deps refs
                                               rtdeps
                                               (and=> (car info)
                                                      (cut * <> 1024))
                                               (cdr info)
                                               fixed-output?
                                               (find-speedup out-path))
                                    result)))
                    vlist-null
                    names)))

      (let ((nodes (vhash-fold (lambda (path node result)
                                 (cons node result))
                               '()
                               nodes)))
        (make-dag nodes

                  ;; sources (nodes with no dependencies)
                  (filter (lambda (node)
                            (let ((path (or (node-derivation-path node)
                                            (node-output-path node))))
                              (not (vhash-assoc path edges))))
                          nodes)

                  ;; sinks (nodes not depended on)
                  (filter (lambda (node)
                            (let ((path (or (node-derivation-path node)
                                            (node-output-path node))))
                              (not (vhash-assoc path back-edges))))
                          nodes))))))

(define (parse-size/duration port)
  "Parse build size and duration info from PORT and return a vhash mapping
output paths to a size/duration pair."
  (define (read/tokenize)
    (let ((line (read-line port)))
      (if (eof-object? line)
          line
          (string-tokenize line))))

  (let loop ((line   (read/tokenize))
             (result vlist-null))
    (match line
      ((size out duration)
       ;; Both the size and duration info are available.
       (let ((size     (string->number size))
             (duration (string->number duration)))
         (if (and size duration) ;; sanity check
             (loop (read/tokenize)
                   (vhash-cons out (cons size duration)
                               result))
             (error "bogus size/duration entry" line))))
      ((size out)
       ;; No duration info, e.g., because it was built by Nix outside of
       ;; Hydra.
       (let ((size (string->number size)))
         (if size
             (loop (read/tokenize)
                   (vhash-cons out (cons size #f) result))
             (error "bogus size entry" line))))
      ((? eof-object?)
       result)
      (_
       (format (current-error-port) "ignoring unrecognized line: `~A'~%"
               line)
       (loop (read/tokenize) result)))))

(define (load-run-time-deps input)
  "Read from INPUT, a text stream where each line contains an output path
followed by the output paths of its run-time dependencies (their output
paths), and return a list of pairs of output paths and run-time
dependencies."
  (let loop ((line   (read-line input))
             (result '()))
    (if (eof-object? line)
        result
        (loop (read-line input)
              (cons (string-tokenize line)
                    result)))))

(define (display-dag-stats dag port)
  "Write to PORT statistics about DAG."
  (let* ((nodes    (dag-nodes dag))
         (sources  (dag-sources dag))
         (sinks    (dag-sinks dag))
         (len      (length nodes))
         (edges    (fold (lambda (n r)
                           (+ (if (node-fixed-output? n)
                                  0
                                  (length (node-prerequisites n)))
                              r))
                         0
                         nodes))
         (size     (count node-size nodes))
         (size*    (count (lambda (n)
                            (and (node-size n)
                                 (> (node-size n) 8192)))
                          nodes))
         (duration (count node-duration nodes))
         (rtdeps   (count (lambda (n)
                            (not
                             (not
                              (force (node-run-time-dependencies n)))))
                          nodes)))
    (format port
            "~a nodes, ~a [~2,1f%] with size info, ~a [~2,1f%] with duration info, ~a edges~%"
            len
            size (* 100.0 (/ size len))
            duration (* 100.0 (/ duration len))
            edges)
    (format port "~a nodes [~2,1f%] with run-time dependency information~%"
            rtdeps (* 100.0 (/ rtdeps len)))
    (format port "~a nodes [~2,1f%] with size > 8 KiB~%"
            size* (* 100.0 (/ size* len)))
    (let ((f (count node-fixed-output? nodes)))
      (format port "~a nodes [~2,1f%] have fixed output (e.g., external downloads)~%"
              f (* 100.0 (/ f len))))
    (let* ((nodes              (remove node-fixed-output? nodes))
           (total              (reduce + 0 (filter-map node-duration nodes)))
           (nodes-with-speedup (remove (compose null? node-speedup) nodes))
           (total-with-speedup (reduce + 0 (filter-map node-duration
                                                       nodes-with-speedup)))
           (s                  (length nodes-with-speedup)))
      (format port "~a nodes [~2,1f% of the comp. nodes, representing ~2,1f% of the total computation] have speedup info~%"
              s (* 100.0 (/ s (length nodes)))
              (* 100.0 (/ total-with-speedup total))))

    (let ((sources (length sources))
          (sinks   (length sinks)))
      (format port "~a sources [~2,1f%], ~a sinks [~2,1f%]~%"
              sources (* 100.0 (/ sources len))
              sinks   (* 100.0 (/ sinks len))))

    (let* ((duration (lambda (n)
                       (if (node-fixed-output? n)
                           0
                           (or (node-duration n) 0))))
           (total    (reduce + 0 (map duration nodes))))
      (format port "total computation time: ~a [~a sec.]~%"
              (seconds->string total) (number->locale-string total)))))


;;;
;;; DAG from a build-events log.
;;;

(define (build-events->dag derivations speedups)
  "Return a DAG based on information in DERIVATIONS and SPEEDUPS,
where DERIVATIONS and SPEEDUPS were obtained using `nix-build-and-log'.

  DERIVATIONS: ((DRV-PATH OUT-PATH FIXED-OUTPUT? DURATION
                 SIZE DEPS RUN-TIME-DEPS) ...)
  SPEEDUPS: ((OUT-PATH ((NB-CORES SPEEDUP ((PHASE . SPEEDUP) ...)) ...)) ...)
"

  (define find-speedup
    (let ((s (fold (lambda (entry result)
                     (match entry
                       ((out-path . speedup)
                        (if (vhash-assoc out-path result)
                            (let ((s (cdr (vhash-assoc out-path result))))
                              ;; Info for OUT-PATH is already available: keep
                              ;; the one with the most speedup data.
                              (if (> (length speedup) (length s))
                                  (vhash-cons out-path speedup result)
                                  result))
                            (vhash-cons out-path speedup result)))))
                   vlist-null
                   speedups)))
      (lambda (out-path)
        ;; output: ((NB-CORES . SPEEDUP) ...)
        (or (and=> (vhash-assoc out-path s)
                   (lambda (cores/speedup)
                     (map (match-lambda
                           ((cores speedup . _)
                            (cons cores speedup)))
                          (cdr cores/speedup))))
            '()))))

  (define out-path->drv-path
    (let ((m (fold (lambda (entry result)
                     (match entry
                       ((drv-path out-path . _)
                        (vhash-cons out-path drv-path result))))
                   vlist-null
                   derivations)))
      (lambda (out-path)
        ;; Return the DRV-PATH associated with OUT-PATH.
        (and=> (vhash-assoc out-path m) cdr))))

  (let-values (((names edges back-edges)
                (apply values
                 (fold (lambda (drv names+edges)
                         (let-values (((names edges back-edges)
                                       (apply values names+edges)))
                           (match drv
                             ((drv-path out-path _ _ _ deps _)
                              (list names
                                    (fold (cut vhash-cons drv-path <> <>)
                                          edges
                                          deps)
                                    (fold (cut vhash-cons <> drv-path <>)
                                          back-edges
                                          deps))))))
                       `(() ,vlist-null ,vlist-null)
                       derivations))))
    (letrec ((find-node (lambda (path)
                          (and=> (vhash-assoc path nodes) cdr)))
             (find-node-by-out-path
              (lambda (out-path)
                ;; Return the node whose output path is OUT-PATH.  Note that
                ;; fixed-output nodes such as scripts have no derivation
                ;; path, hence the `or'.
                (or (and=> (out-path->drv-path out-path) find-node)
                    (find-node out-path))))
             (nodes
              (fold (lambda (drv result)
                      (match drv
                        ((drv-path out-path fixed-output? duration size
                          _ rt-deps)
                         (if (vhash-assoc drv-path result)
                             (let ((prev (cdr (vhash-assoc drv-path result))))
                               (format (current-error-port)
                                       "`~a' encountered more than once; only the first occurrence is kept: ~s~%"
                                       drv-path prev)
                               result)
                             (let* ((deps
                                     (delay
                                       ;; Return the nodes pointed to by
                                       ;; OUT-PATH.  The effect is that real
                                       ;; sources---i.e., files that appear as
                                       ;; sources in `.drv's, such as
                                       ;; `download.sh', etc.---are omitted from
                                       ;; the resulting DAG.  This is OK since
                                       ;; they represent no computation and
                                       ;; negligible data transfers.
                                       (vhash-fold* (lambda (dep result)
                                                      (or (and=>
                                                           (find-node dep)
                                                           (cut cons <> result))
                                                          result))
                                                    '()
                                                    drv-path
                                                    edges)))
                                    (refs
                                     (delay
                                       ;; Return the nodes pointing to OUT-PATH.
                                       (vhash-fold* (lambda (ref result)
                                                      (or (and=>
                                                           (find-node ref)
                                                           (cut cons <> result))
                                                          result))
                                                    '()
                                                    drv-path
                                                    back-edges)))

                                    (rt-deps
                                     (delay
                                       (and
                                        rt-deps
                                        (let ((direct
                                               (append (force deps)
                                                       (append-map
                                                        node-recursive-run-time-dependencies
                                                        (force deps)))))
                                          (filter (lambda (d)
                                                    ;; Keep only direct deps.
                                                    (memq d direct))
                                                  (map find-node-by-out-path
                                                       (remove
                                                        (cut string=?
                                                             out-path <>)
                                                        rt-deps))))))))

                               ;; There can be several drv paths mapped to one
                               ;; output path (e.g., because the same tarball is
                               ;; downloaded once with the boostrap environment and
                               ;; then with the final environment), but we always
                               ;; keep all of them.
                               (vhash-cons drv-path
                                           (make-node out-path drv-path deps refs
                                                      rt-deps
                                                      size
                                                      duration
                                                      fixed-output?
                                                      (find-speedup out-path))
                                           result))))))
                    vlist-null
                    derivations)))

      (let* ((nodes   (vhash-fold (lambda (path node result)
                                    (cons node result))
                                  '()
                                  nodes))
             (sources (future
                       (filter (compose null? force node-dependencies)
                               nodes)))
             (sinks   (future
                       (filter (compose null? force node-referrers)
                               nodes))))
        (make-dag nodes (touch sources) (touch sinks))))))


;;;
;;; Convenience.
;;;

(define (load-nix-dag dag-xml-file size/duration-file
                      derivation->output-file
                      fixed-output-derivation-file
                      run-time-deps-file
                      speedup-file)
  "Load a Nix DAG from the given data files."
  (for-each (lambda (file)
              (if (and file (newer? dag-xml-file file))
                  (format (current-error-port) "warning: `~a' is newer than `~a'~%"
                          dag-xml-file file)))
            (list derivation->output-file
                  fixed-output-derivation-file))

  (let* ((sxml (phase "loading `~a'" dag-xml-file
                      (xml->sxml (open-input-file dag-xml-file))))
         (s/d  (phase "loading `~a'" size/duration-file
                      (parse-size/duration
                       (open-input-file size/duration-file))))
         (d/o  (phase "loading `~A'" derivation->output-file
                      (with-input-from-file derivation->output-file
                        read)))
         (fo   (phase "loading `~a'" fixed-output-derivation-file
                      (with-input-from-file fixed-output-derivation-file
                        read)))
         (rtd  (if run-time-deps-file
                   (phase "loading `~a'" run-time-deps-file
                          (load-run-time-deps
                           (open-input-file run-time-deps-file)))
                   '()))
         (s    (if speedup-file
                   (phase "loading `~a'" speedup-file
                          (with-input-from-file speedup-file read))
                   '()))
         (dag  (phase "building initial DAG"
                      (nixpkgs->dag sxml s/d d/o fo rtd s))))
    dag))


;;;
;;; DAX output flop calibration.
;;;

(define (dax-flops/second-ratio)
  "Return the factor by which `SD_daxload' multiplies execution
times (seconds) to obtain a task's number of FLOPs (amount of computation).
See
`http://lists.gforge.inria.fr/pipermail/simgrid-user/2010-May/001987.html'
for details."

  (define computation
    ;; The amount of computation and CPU power used for the test.  This could
    ;; be anything.
    1e6)

  (let* ((platform (make-sxml-platform 1 '(0 . 0)
                                       (cons computation computation)
                                       '(0.0 . 0.0) '(0.0 . 0.0)))
         (name     (symbol->string (gensym "hubble-task-")))
         (node     (make-node name #f (delay '()) (delay '()) (delay '()) 0
                              computation #f))
         (dag      (make-dag (list node) (list node) (list node)))
         (sdax     (dag->sxml/dax dag #:convert-duration identity)))
    (with-temporary-file
     (lambda (dax-file)
       (with-output-to-file dax-file
         (lambda ()
           (sxml->xml sdax)))

       (with-temporary-file
        (lambda (platform-file)
          (with-output-to-file platform-file
            (lambda ()
              (sxml->xml platform)))

          (simdag-init! '())
          (make-simdag-environment platform-file)

          (let ((tasks (simdag-load-dax dax-file)))
            (for-each (cute simdag-auto-schedule-task <>
                            (simdag-workstations))
                      (filter (lambda (t)
                                (eq? (simdag-task-kind t)
                                     'sequential-computation))
                              tasks))
            (let* ((done (simdag-simulate -1))
                   (mine (assert simdag-task?
                                 (find (lambda (t)
                                         (and (eq? (simdag-task-kind t)
                                                   'sequential-computation)
                                              (string-contains
                                               (simdag-task-name t) name)))
                                       done)))
                   (span (- (simdag-task-finish-time mine)
                            (simdag-task-start-time mine))))
              (simdag-exit!)

              ;; Since the CPU power is equal to COMPUTATION, the execution
              ;; time should be 1.0.
              span))))))))

;;; Local Variables:
;;; eval: (put 'sxml-match 'scheme-indent-function 1)
;;; End:
