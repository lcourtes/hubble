;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; -*- coding: utf-8 -*-
;;; Copyright (C) 2010, 2011 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (hubble output)
  #:use-module (hubble dag)
  #:use-module (hubble scheduling)
  #:use-module (hubble simulation)
  #:use-module ((hubble utils) #:select (exponential?))
  #:use-module (simgrid)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (ice-9 pretty-print)
  #:export (display-simulation-stats
            simdag->pajé

            data-sets->plotutils
            data-sets->gnuplot
            simulation-result->data-sets))


;;;
;;; Simulation statistics.
;;;

(define (display-simulation-stats dag tasks workstations)
  "Display simulation stats based on TASKS, a list of tasks executed on
WORKSTATIONS."

  (let ((end   (total-duration tasks))
        (ct    (total-computation-time tasks))
        (tt    (total-transfer-time tasks))
        (busy  (workstation-computation-time workstations tasks))
        (cpmin (minimum-critical-path-length dag workstations)))
    (format #t "~%~A tasks (out of ~A computational tasks) changed states~%"
            (length tasks) (length (dag-nodes dag)))
    (let ((m (maximum-upward-rank dag workstations)))
      (format #t "total duration:             ~10,4f [~2,2fx max upward rank, ~9,2f]~%"
              end (/ end m) m)
      (format #t "min critical path length:   ~10,4f~%" cpmin)
      (format #t "schedule length ratio:      ~10,4f~%" (/ end cpmin))
      (format #t "speedup:                    ~10,4f [for ~a workstations]~%"
              (/ (minimum-sequential-computation-time dag workstations)
                 end)
              (length workstations)))

    (format #t "cumulated computation time: ~10,4f~%" ct)
    (format #t "cumulated transfer time:    ~10,4f~%" tt)
    (format #t "average transfer stretch:   ~10,4f~%"
            (transfer-average-stretch tasks))

    (let ((xfer (data-transferred tasks)))
      (format #t "data transfered:            ~10,4f MiB~%"
              (/ xfer (* 1024 1024.0))))

    (for-each (lambda (ws+time)
                (let ((ws   (first ws+time))
                      (time (second ws+time)))
                  (format #t "~15a computed ~2,2f% of the time [~9,2f]~%"
                          (simdag-workstation-name ws)
                          (* 100.0 (/ time end))
                          time)))
              busy)
    (let ((total (apply + (map second busy))))
      (format #t "overall:        computed ~2,2f% of the time [~9,2f]~%"
              (* 100.0 (/ total (* end (length workstations))))
              total))))


;;;
;;; Traces.
;;;

;; Pajé format, see
;; <http://www-id.imag.fr/Logiciels/paje/publications/files/lang-paje.pdf>,
;; "Pajé trace file format", B. de Oliveira Stein and J. Chassin de
;; Kergommeaux.

(define (simdag->pajé tasks workstations port)
  "Write to PORT a Pajé trace of the simulation of TASKS.  The trace can then
be used as input to visualization tools such as ViTE, which will show a Gantt
diagram with arrows showing communications, etc."

  (define (prologue)
    (format port
"%EventDef PajeDefineContainerType 1
% Alias string
% ContainerType string
% Name string
%EndEventDef
%EventDef PajeDefineStateType 3
% Alias string
% ContainerType string
% Name string
%EndEventDef
%EventDef PajeDefineEntityValue 6
% Alias string
% EntityType string
% Name string
% Color color
%EndEventDef
%EventDef PajeCreateContainer 7
% Time date
% Alias string
% Type string
% Container string
% Name string
%EndEventDef
%EventDef PajeDestroyContainer 8
% Time date
% Name string
% Type string
%EndEventDef
%EventDef PajeSetState 10
% Time date
% Type string
% Container string
% Value string
% Tasks string
%EndEventDef
%EventDef PajeDefineLinkType 15
% Alias string
% Name string
% ContainerType string
% SourceContainerType string
% DestContainerType string
%EndEventDef
%EventDef PajeDefineVariableType 50
% Alias string
% Name  string
% ContainerType string
%EndEventDef
%EventDef PajeSetVariable 51
% Time date
% Type string
% Container string
% Value double
%EndEventDef
%EventDef PajeAddVariable 52
% Time date
% Type string
% Container string
% Value double
%EndEventDef
%EventDef PajeSubVariable 53
% Time date
% Type string
% Container string
% Value double
%EndEventDef
%EventDef PajeStartLink 60
% Time date
% Type string
% Container string
% SourceContainer string
% Value string
% Key string
%EndEventDef
%EventDef PajeEndLink 61
% Time date
% Type string
% Container string
% DestContainer string
% Value string
% Key string
%EndEventDef
1 CT_BuildFarm  0            \"build farm\"
1 CT_Machine    CT_BuildFarm \"machine\"
1 CT_Core       CT_Machine   \"core\"
3 ST_CoreState  CT_Core      \"core state\"
6 S_Idle        ST_CoreState \"idle\" \"1.0 0.0 0.0\"
6 S_Busy        ST_CoreState \"busy\" \"0.0 1.0 0.0\"
15 LT_MachineLink  \"data transfer\" CT_BuildFarm CT_Core CT_Core
"))

  (define (machine-name ws)
    (or (simdag-workstation-property ws "machine")
        (simdag-workstation-name ws)))

  (define (core-name ws)
    (let ((m (simdag-workstation-property ws "machine"))
          (c (simdag-workstation-property ws "core")))
      (if (and m c)
          (format #f "core ~a" c)
          (simdag-workstation-name ws))))

  (define (declare-workstations)
    (format port
            "7 0.0 C_NixOSDotOrg CT_BuildFarm 0 \"build farm\"~%")
    (for-each (lambda (m)
                (format port "7 0.0 \"C_M_~a\" CT_Machine C_NixOSDotOrg \"~a\"~%"
                        m m))
              (delete-duplicates (map machine-name workstations)))
    (for-each (lambda (ws)
                (let ((machine     (machine-name ws))
                      (pretty-name (core-name ws))
                      (name        (simdag-workstation-name ws)))
                  (format port "7 0.0 \"C_~a\" CT_Core \"C_M_~a\" \"~a\"~%"
                          name machine pretty-name)
                  (format port "10 0.0 ST_CoreState \"C_~a\" S_Idle \"(start)\"~%"
                          name)))
              workstations))

  (define (epilogue end)
    (for-each (lambda (ws)
                (let ((name (simdag-workstation-name ws)))
                  (format port "8 ~f \"C_~a\" CT_Core~%"
                          end name)))
              workstations)
    (for-each (lambda (m)
                (format port "8 ~f \"C_M_~a\" CT_Machine~%"
                        end m))
              (delete-duplicates (map machine-name workstations)))
    (format port "8 ~f C_NixOSDotOrg CT_BuildFarm~%"
            end))

  (define (declare-events events)
    (let ((events (sort events
                        (lambda (event1 event2)
                          (match event1
                            ((type1 date1 _ task1)
                             (match event2
                               ((_ (? (cut = date1 <>)) _ (? (cut eq? task1 <>)))
                                ;; Preserve causality: `start' occurs before
                                ;; `end'.
                                (case type1
                                  ((start-build start-transfer) #t)
                                  (else                         #f)))
                               ((_ date2 _ _)
                                (< date1 date2)))))))))
      (with-output-to-file "events.txt"
        (lambda ()
          ;; XXX: For debugging purposes.
          (setvbuf (current-output-port) _IOFBF 32768)
          (pretty-print events)))

      (fold (lambda (event schedule)
              (match event
                (('start-build time ws task)
                 (let* ((prev-state (cdr (assq ws schedule)))
                        (new-state  (cons task prev-state)))
                   (format port "10 ~f ST_CoreState \"C_~a\" S_Busy \"~a\"~%"
                           time (simdag-workstation-name ws)
                           (string-join (map simdag-task-name new-state)))
                   (alist-cons ws new-state (delq ws schedule))))
                (('end-build time ws task)
                 (let* ((prev-state (cdr (assq ws schedule)))
                        (new-state  (delq task prev-state)))
                   (if (null? new-state)
                       (format port "10 ~f ST_CoreState \"C_~a\" S_Idle \"(idle)\"~%"
                               time (simdag-workstation-name ws))
                       (format port "10 ~f ST_CoreState \"C_~a\" S_Busy \"~a\"~%"
                               time (simdag-workstation-name ws)
                               (string-join (map simdag-task-name new-state))))
                   (alist-cons ws new-state (delq ws schedule))))
                (('start-transfer time (source . dest) task)
                 ;; XXX: ViTE apparently fails to correctly display
                 ;; machine-to-machine links so we instead use core-to-core
                 ;; links.
                 (if (not (eq? source dest))
                     (format port "60 ~f LT_MachineLink \"C_NixOSDotOrg\" \"C_~a\" \"~a\" \"~x\"~%"
                             time
                             (simdag-workstation-name source)
                             (simdag-task-name task)
                             (object-address task)))
                 schedule)
                (('end-transfer time (source . dest) task)
                 (if (not (eq? source dest))
                     (let ((dest (simdag-workstation-name dest)))
                       ;; The Pajé reference reads: "A link is defined by two
                       ;; events, a “PajeStartLink” and a
                       ;; “PajeEndLink”. These two events are matched and
                       ;; considered to form a link when their respective
                       ;; “Container”, “Value” and “Key” fields are the
                       ;; same."
                       (format port "61 ~f LT_MachineLink \"C_NixOSDotOrg\" \"C_~a\" \"~a\" \"~x\"~%"
                               time
                               dest
                               (simdag-task-name task)
                               (object-address task))))
                 schedule)))
            (map list workstations)
            events)))

  (define (tasks->events tasks)
    (fold (lambda (task events)
            (let ((start (simdag-task-start-time task))
                  (end   (simdag-task-finish-time task)))
              (case (simdag-task-kind task)
                ((sequential-computation)
                 (let ((ws (match (simdag-task-workstations task)
                             (((and (? simdag-workstation?) ws))
                              ;; TASK is assumed to be sequential and should
                              ;; have been handled by exactly one workstation.
                              ws)
                             (_
                              (format (current-error-port)
                                      "task `~A' not handled by exactly one workstation: ~A~%"
                                      task
                                      (simdag-task-workstations task))
                              #f))))
                   (if (and (>= start 0.0) (>= end 0.0) ws)
                       (cons* (list 'start-build start ws task)
                              (list 'end-build   end ws task)
                              events)
                       (begin
                         (format (current-error-port)
                                 "ignoring task `~A' (~A, ~A,~A)~%"
                                 task start end ws)
                         events))))
                ((end-to-end-communication)
                 (let ((ws (match (simdag-task-workstations task)
                             ((source dest) (cons source dest))
                             (_
                              (format (current-error-port)
                                      "e2e task `~A' not assigned to exactly two workstations: ~A~%"
                                      task (simdag-task-workstations task))
                              #f))))
                   (if (and (>= start 0.0) (>= end 0.0) ws)
                       (cons* (list 'start-transfer start ws task)
                              (list 'end-transfer   end ws task)
                              events)
                       events)))
                (else
                 (format (current-error-port)
                         "ignoring task `~A' of unknown kind `~A'~%"
                         task (simdag-task-kind task))
                 events))))
          '()
          tasks))

  (prologue)
  (declare-workstations)
  (let* ((events (tasks->events tasks))
         (end    (fold (lambda (event end)
                         (match event
                           ((_ date _ _)
                            (if (> date end) date end))))
                       0.0
                       events)))
    (declare-events events)
    (epilogue end)))


;;;
;;; Plots.
;;;

(define (data-sets->plotutils name x-label y-label data-sets port)
  "Write DATA-SETS in the format suitable to GNU Plotutil's `graph'
program'."
  (for-each (lambda (data-set)
              (format port "# algo `~A'~%" (car data-set))
              (for-each (lambda (coord)
                          (format port "~A ~A~%"
                                  (first coord) (second coord)))
                        (cdr data-set))
              (newline port))
            data-sets))

(define* (data-sets->gnuplot name x-label y-label data-sets port
                             #:key (allow-log-scale? #t))
  "Write DATA-SETS in the format suitable to Gnuplot (blech!).  Log scale is
forbidden for histograms, for instance, so in that case you may want to pass
#f as #:ALLOW-LOG-SCALE?."
  (define algorithms
    (map car data-sets))

  (format port "set terminal postscript enhanced color~%")
  (format port "set grid~%")
  (format port "set title \"~A\"~%set xlabel \"~A\"~%set ylabel \"~A\"~%"
          name x-label y-label)

  (if (and allow-log-scale?
           (every (lambda (data-set)
                    (exponential? (map first (cdr data-set))))
                  data-sets))
      (format #t "set logscale x~%"))
  (if (and allow-log-scale?
           (every (lambda (data-set)
                    (exponential? (map second (cdr data-set))))
                  data-sets))
      (format #t "set logscale y~%"))

  (format port "plot ")
  (format port "~A~%"
          (string-join
           (map (cut format #f "'-' title \"~a\" with linespoints" <>)
                algorithms)
           ", "))

  (for-each (lambda (data-set)
              (format port "# algo `~A'~%" (car data-set))
              (for-each (lambda (coord)
                          (format port "~A ~A~%"
                                  (first coord) (second coord)))
                        (cdr data-set))
              (format port "e~%"))
            data-sets))


;;;
;;; Data set extraction.
;;;

(define (simulation-result->data-sets labels results coordinates)
  "Given RESULTS, a list of lists of `simulate' results obtained for LABELS
and for different platforms, return a list of \"data sets\", each
corresponding to one of LABELS.  The coordinates are obtained by applying
COORDINATES to each `simulate' result.

RESULTS has the following form:

  ((PLATFORM1-LABEL1 PLATFORM1-LABEL2 ...) ...)

The return value looks like:

  ((LABEL . ((ABSCISSA ORDINATE) ...)) ...)"

  (map (lambda (data-set)
         ;; Sort DATA-SET by increasing abscissa.
         (cons (car data-set)
               (sort (cdr data-set)
                     (lambda (c1 c2)
                       (< (first c1) (first c2))))))
       (unfold (cute >= <> (length labels))
               (lambda (i)
                 (cons (list-ref labels i)
                       (map (lambda (per-platform)
                              (coordinates (list-ref per-platform i)))
                            results)))
               1+
               0)))
