;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (hubble platform)
  #:use-module ((hubble utils) #:select (cartesian-product assert))
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (make-sxml-platform))


(define %default-random-state
  (seed->random-state (+ (car (gettimeofday)) (cdr (gettimeofday)))))

(define (random-in-range min max state)
  "Return a random number between MIN and MAX included."
  (if (= min max)
      min
      (+ min (random (* (+ 1.0 1e-6) (- max min)) state))))

(define* (make-sxml-platform machines cores power bandwidth latency
                             #:optional (chance %default-random-state))
  "Return the SXML tree of a SimGrid platform description with MACHINES
machines.  Each machine has a random number of cores between 2^(car CORES)
and 2^(cdr CORES), per-core computational power between (car POWER) and (cdr
POWER) instructions per second, etc.
"
  ;; FIXME: Return only 1 value to work around
  ;; <https://savannah.gnu.org/bugs/index.php?31445>.
  ;; "In addition, return 3 other values: the list of CPU powers, the list of link
  ;; bandwidths, and the list of link latencies.  This is useful when they are
  ;; chosen at random."

  (define loopback-bandwidth
    ;; The throughput of a "local transfer".  Actually, for us, "local
    ;; transfers" model accesses to a shared Nix store on a multi-core
    ;; system, so there is no actual transfer.
    "1e30")

  (define cpu-powers
    (unfold (cut >= <> machines)
            (lambda (i)
              (random-in-range (car power) (cdr power) chance))
            1+
            0))

  (define link-bandwidths
    (unfold (cut >= <> machines)
            (lambda (i)
              (random-in-range (car bandwidth) (cdr bandwidth) chance))
            1+
            0))

  (define link-latencies
    (unfold (cut >= <> machines)
            (lambda (i)
              (random-in-range (car latency) (cdr latency) chance))
            1+
            0))

  (define (key=value? pair)
    (equal? (first pair) (second pair)))

  (define (machine-id m c)
    (format #f "machine~A/core~A" m c))

  (define (make-host id c)
    `(host (@ (id ,(machine-id id c))
              (power ,(list-ref cpu-powers id)))

           ;; Use the `machine' and `core'
           ;; properties to denote the fact that
           ;; this is one core of a multi-core
           ;; machine.
           (prop (@ (id "machine")
                    (value
                     ,(format #f "machine~A"
                              id))))
           (prop (@ (id "core")
                    (value
                     ,(number->string c))))))

  (assert (lambda (x)
            (and (number? x) (> x 0)))
          machines)

  (let ((mids   (iota machines)) ;; machine identifiers
        (mcores (unfold (cut >= <> machines)
                        (lambda (_)
                          (expt 2 (random-in-range (car cores) (cdr cores)
                                                   chance)))
                        1+
                        0)))
    `(*TOP*
      (*PI* xml "version='1.0'")
      ,(lambda () ;; hack
         (display "<!DOCTYPE platform SYSTEM \"simgrid.dtd\">"))

      (platform
       (@ (version "3"))

       (AS (@ (id "AS0") (routing "Full"))        ; new in SimGrid 3.5
           ,@(append-map (lambda (id cores) ;; hosts
                           (unfold (cut >= <> cores)
                                   (cut make-host id <>)
                                   1+
                                   0))
                         mids
                         mcores)

           "\n"
           ,@(append-map (lambda (mid b l) ;; loopback, in, and out links
                           `("\n"
                             (link (@ (id ,(format #f "machine~A:lo" mid))
                                      (bandwidth ,loopback-bandwidth)
                                      (latency "0.000015")))
                             (link (@ (id ,(format #f "machine~A:in" mid))
                                      (bandwidth ,(number->string b))
                                      (latency ,(number->string l))))
                             (link (@ (id ,(format #f "machine~A:out" mid))
                                      (bandwidth ,(number->string b))
                                      (latency ,(number->string l))))))
                         mids
                         link-bandwidths
                         link-latencies)

           "\n"
           ,@(append-map (lambda (mid cores) ;; loopback routes
                           (let ((cid (iota cores)))
                             (fold (lambda (src+dst result)
                                     `((route
                                        (@ (src ,(machine-id mid (first src+dst)))
                                           (dst ,(machine-id mid (second src+dst)))
                                           (symmetrical "NO")) ; new in 3.5
                                        (link_ctn
                                         (@ (id ,(format #f "machine~A:lo" mid)))))
                                       ,@result))
                                   '()
                                   (remove key=value?
                                           (cartesian-product
                                            (list cid cid))))))
                         mids
                         mcores)

           "\n"
           ,@(append-map (lambda (msrc+mdst)
                           (let ((m1 (first msrc+mdst))
                                 (m2 (second msrc+mdst)))
                             (map (lambda (csrc+cdst)
                                    `(route
                                      (@ (src ,(machine-id m1 (first csrc+cdst)))
                                         (dst ,(machine-id m2 (second csrc+cdst)))
                                         (symmetrical "NO")) ; new in 3.5
                                      "\n"
                                      (link_ctn
                                       (@ (id ,(format #f "machine~A:out" m1))))
                                      (link_ctn
                                       (@ (id ,(format #f "machine~A:in" m2))))))
                                  (cartesian-product
                                   (list (iota (list-ref mcores m1))
                                         (iota (list-ref mcores m2)))))))
                         (remove key=value?
                                 (cartesian-product (list mids mids)))))))))
