;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010, 2011, 2012 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (hubble scheduling)
  #:use-module (hubble utils)
  #:use-module (hubble dag)
  #:use-module (simgrid)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-60)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 pretty-print)
  #:export (select-first-task
            select-first-workstation
            select-many-cores
            select-2-cores
            select-4-cores
            select-8-cores
            select-random-task
            select-random-workstation

            dag->heft-ranking
            maximum-upward-rank

            make-heft-task-selector
            select-workstation/heft
            select-workstation/m-heft

            same-machine?
            same-machine-workstations
            workstations->machines+cores
            seconds->flops
            measured-node-speedup
            measured-node-speedup*
            sublinear-node-speedup
            dynamic-scheduling

            critical-path
            minimum-critical-path-length
            minimum-sequential-computation-time))


;;;
;;; Simple greedy scheduling.
;;;

(define (select-first-workstation idle workstations node
                                  cached? ongoing-transfers
                                  node-speedup)
  "Select the first item of IDLE (the list of idle workstations) to
execute NODE."
  (define (needs-transfer? n ws)
    ;; True if the result of N needs to be transferred to WS.
    (let ((size (node-size n)))
      (and size (> size 0)
           (not (cached? n ws)))))

  (define (workstation-that-stores n)
    ;; Return the first workstation that stores the result of N.
    (assert (find (cut cached? n <>) workstations)))

  (let* ((ws    (car idle))
         (deps  (filter (cut needs-transfer? <> ws)
                        (node-prerequisites node))))
    (values ws
            (map (lambda (d)
                   (cons d (workstation-that-stores d)))
                 deps))))

(define (select-first-task ready)
  "Select the first node among READY."
  (values (car ready) (cdr ready)))

(define (select-many-cores idle workstations node
                           cached? ongoing-transfers
                           node-speedup)
  "Select as many cores among IDLE that belong to the same machine."
  (define (needs-transfer? n ws)
    ;; True if the result of N needs to be transferred to WS.
    (let ((size (node-size n)))
      (and size (> size 0)
           (not (cached? n ws)))))

  (define (workstation-that-stores n)
    ;; Return the first workstation that stores the result of N.
    (assert (find (cut cached? n <>) workstations)))

  (define machine-cores
    (workstations->machines+cores idle))

  (define duration
    (node-duration node))

  (let* ((ws+   (if duration
                    (highest (map cdr machine-cores) length <)
                    (list (car idle))))
         (deps  (filter (cute needs-transfer? <> (car ws+))
                        (node-prerequisites node))))
    (values ws+
            (map (lambda (d)
                   (cons d (workstation-that-stores d)))
                 deps))))

(define (select-n-cores core-number)
  "Return a scheduling strategy that select CORE-NUMBER cores of a given
machine for each task."
  (lambda (idle workstations node
           cached? ongoing-transfers
           node-speedup)
    "Select as CORE-NUMBER cores among IDLE that belong to the same machine."
    (define (needs-transfer? n ws)
      ;; True if the result of N needs to be transferred to WS.
      (let ((size (node-size n)))
        (and size (> size 0)
             (not (cached? n ws)))))

    (define (workstation-that-stores n)
      ;; Return the first workstation that stores the result of N.
      (assert (find (cut cached? n <>) workstations)))

    (define machine-cores
      (workstations->machines+cores idle))

    (let* ((ws+   (take (cdar machine-cores)      ; cores of the 1st machine
                        core-number))
           (deps  (filter (cute needs-transfer? <> (car ws+))
                          (node-prerequisites node))))
      (values ws+
              (map (lambda (d)
                     (cons d (workstation-that-stores d)))
                   deps)))))

(define select-2-cores (select-n-cores 2))
(define select-4-cores (select-n-cores 4))
(define select-8-cores (select-n-cores 8))



;;;
;;; Random greedy scheduling.
;;;

(define (select-random-workstation idle workstations node
                                   cached? ongoing-transfers
                                   node-speedup)
  "Select the first item of IDLE (the list of idle workstations) to
execute NODE."
  (define (needs-transfer? n ws)
    ;; True if the result of N needs to be transferred to WS.
    (let ((size (node-size n)))
      (and size (> size 0)
           (not (cached? n ws)))))

  (define (workstation-that-stores n)
    ;; Return a random workstation that stores the result of N.
    (let ((ws (assert pair? (filter (cut cached? n <>) workstations))))
      (random-ref ws)))

  (let* ((ws    (car idle))
         (deps  (filter (cut needs-transfer? <> ws)
                        (node-prerequisites node))))
    (values ws
            (map (lambda (d)
                   (cons d (workstation-that-stores d)))
                 deps))))

(define (select-random-task ready)
  "Select a random node among READY."
  (let ((t (random-ref ready)))
    (values t (delq t ready))))


;;;
;;; HEFT.
;;;

(define (dag->heft-ranking dag workstations)
  "Compute the upward/downward rank of the HEFT algorithm of each node of
DAG; return two procedures: the first one maps a node to its upward rank, and
the second one maps a node to its downward rank.  The ranks are computed
using the average execution time of the tasks on WORKSTATIONS."

  (define (predecessors n)
    (force (node-dependencies n)))
  (define (successors n)
    (force (node-referrers n)))
  (define (average-execution-time n)
    (if (node-duration n)
        (let ((c (seconds->flops (node-duration n))))
          (/ (apply +
                    (map (cut simdag-workstation-computation-time <> c)
                         workstations))
             (length workstations)))
        0))
  (define average-transfer-time
    ;; Average time to transfer the result of N between any pair of elements
    ;; in WORKSTATIONS.
    (let* ((routes    (cartesian-product (make-list 2 workstations)))
           (pairs     (expt (length workstations) 2))
           (latency   (/ (reduce + 0.0
                                 (map (cut apply
                                           simdag-communication-latency <>)
                                      routes))
                         pairs))
           (bandwidth (/ (reduce + 0.0
                                 (map (cut apply
                                           simdag-communication-bandwidth <>)
                                      routes))
                         pairs)))
      (lambda (n)
        (let ((size (node-size n)))
          (if size
              ;; Compute a rough approximation of the average of
              ;; `simdag-communication-time' for SIZE applied to each one of
              ;; ROUTES.  The advantage is that computing the approximation
              ;; is O(1).
              (+ latency (/ size bandwidth))
              0)))))

  (letrec ((upward-rank
            (lambda (n)
              (force (cdr (vhash-assq n upward)))))
           (downward-rank
            (lambda (n)
              (force (cdr (vhash-assq n downward)))))
           (upward
            (fold (lambda (n r)
                    (vhash-consq n
                     (delay
                       (+ (average-execution-time n)
                          (if (null? (successors n))
                              0
                              (apply max
                                     (map (let ((t (average-transfer-time n)))
                                            (lambda (s)
                                              (+ t (upward-rank s))))
                                          (successors n))))))
                     r))
                  vlist-null
                  (dag-nodes dag)))
           (downward
            (fold (lambda (n r)
                    (vhash-consq n
                     (delay
                       (if (null? (predecessors n))
                           0
                           (apply max
                                  (map (lambda (p)
                                         (+ (average-execution-time p)
                                            (average-transfer-time p)
                                            (downward-rank p)))
                                       (predecessors n)))))
                     r))
                  vlist-null
                  (dag-nodes dag))))
    (values upward-rank downward-rank)))

(define (maximum-upward-rank dag workstations)
  "Return the maximum upward rank of DAG, i.e., the length of the critical
path.  When the workstations used are all identical, this is the smallest
possible execution time of DAG, omitting the time it takes to transfer data."
  (let ((upward-rank (dag->heft-ranking dag workstations)))
    (apply max (map upward-rank (dag-sources dag)))))

(define (make-heft-task-selector dag workstations)
  "Return a procedure that selects from a ready list the task with the
highest HEFT upward rank."
  (let* ((upward-rank (dag->heft-ranking dag workstations))
         (node>       (lambda (n1 n2)
                        (> (upward-rank n1) (upward-rank n2))))
         (nodes       (sort-list (dag-nodes dag) node>)))
    (lambda (ready)
      (let ((n (find (cut memq <> ready) nodes)))
       (values n (delq n ready))))))

(define (select-workstation/heft* idle workstations node
                                  cached? ongoing-transfers
                                  node-speedup
                                  parallel-tasks?)
  "Select the workstation among IDLE to build NODE that minimizes the
earliest finish time (EFT) of NODE, taking into account communication costs.
In addition, this procedure selects the source of data transfers such that
communication time is minimized.

When PARALLEL-TASKS? is true, check to see if using parallel tasks can
improve the EFT; otherwise behave as though all the machines have only one
core."
  (define prerequisites
    (node-prerequisites node))

  (define (needs-transfer? n ws)
    ;; True if the result of N needs to be transferred to WS.
    (let ((size (node-size n)))
      (and size (> size 0)
           (not (cached? n ws)))))

  (define workstations-that-store
    (let ((node->ws (fold (lambda (d r)
                            (vhash-consq d
                                         (filter (cut cached? d <>)
                                                 workstations)
                                         r))
                          vlist-null
                          prerequisites)))
      (lambda (n)
        ;; Return the list of workstations that store N, a dependency of NODE.
        (and=> (vhash-assq n node->ws) cdr))))

  (define (communication-time src dst size)
    ;; Return an estimate of the time to transfer SIZE bytes from SRC to DST,
    ;; accounting for contention at SRC's output link and at DST's input
    ;; link.
    (define (outgoing? t ws)
      (let ((src (simdag-task-workstation-ref t 0)))
        (eq? src ws)))

    (let ((t (simdag-communication-time src dst size))
          (c (count (cut outgoing? <> src)
                    (ongoing-transfers src))))
      ;; FIXME: Ignore C since it seems to make things worse.
      (* (+ 1.0 (* 0.0 c)) t)))

  (define (closest-workstation dst workstations size)
    ;; Return item of WORKSTATIONS that's the closest to DST in terms of
    ;; time needed to transfer SIZE bytes.  When the communication time from
    ;; DST to several of WORKSTATIONS is the same, choose a random one
    ;; from the candidate workstations, so as to avoid overloading a single
    ;; link.
    (highest/randomized workstations
                        (cut communication-time <> dst size)
                        >))

  (define (fastest-transfer ws)
    ;; Return the fastest transfer for WS and its duration.  A transfer is a
    ;; list of dependency/workstation pair: ((DEP1 . SRC1) (DEP2 . SRC2) ...).
    (let ((deps (filter (cut needs-transfer? <> ws) prerequisites)))
      (map (lambda (d)
             (cons d
                   (closest-workstation ws
                                        (workstations-that-store d)
                                        (node-size d))))
           deps)))

  (define (transfer-time ws xfer)
    ;; Return the time it takes to do transfers XFER to workstation WS.  XFER
    ;; is a list of node/workstation pair, where the node's result is what
    ;; should be transferred and the workstation the source of the transfer.
    ;; Approximate it as the sum of the transfer times, since there'd be
    ;; contention on WS's input link anyway.
    (reduce + 0.0
            (map (lambda (n+src)
                   (let ((n   (car n+src))
                         (src (cdr n+src)))
                     (communication-time src ws
                                         (assert number? (node-size n)))))
                 xfer)))

  (define (finish-time ws+ xfers)
    ;; Return the finish time (read: duration) of running COMPUTATION on WS+,
    ;; after doing all XFERS.
    (+ (transfer-time (car ws+) xfers)
       (/ (simdag-workstation-computation-time (car ws+) computation)
          (node-speedup node (length ws+)))))

  (define (fastest-workstation+transfers)
    ;; Return the workstation among IDLE that minimizes the earliest finish
    ;; time (EFT), along with the associated dependency transfers.  When
    ;; multiple cores of a single machine are available, compute the EFT for
    ;; each possible number of cores.
    (let ((ws+xfer (if parallel-tasks?
                       (append-map (lambda (m)
                                     (let* ((cores     (cdr m))
                                            (max-cores (length cores))
                                            (xfer      (fastest-transfer
                                                        (car cores))))
                                       (unfold (cut > <> max-cores)
                                               (lambda (c)
                                                 (cons (take cores c)
                                                       xfer))
                                               1+
                                               1)))
                                   machine-cores)
                       (map (lambda (m)
                              (let ((ws (cadr m)))
                                (cons (list ws)
                                      (fastest-transfer ws))))
                            machine-cores))))
      (car+cdr
       (highest/randomized ws+xfer
                           (lambda (ws+xfer)
                             (finish-time (car ws+xfer) (cdr ws+xfer)))
                           >))))

  (define machine-cores
    ;; (("M1" c1 c2 c3 c4) ("M2" c1 c2) ...)
    (workstations->machines+cores idle))

  (define computation
    ;; The computation amount for NODE.
    (seconds->flops (or (node-duration node) 0)))

  (fastest-workstation+transfers))

(define select-workstation/heft
  ;; A variant of HEFT that uses only sequential tasks.
  (cut select-workstation/heft* <> <> <> <> <> <> #f))

(define select-workstation/m-heft
  ;; A variant of HEFT that uses parallel tasks: the "mixed-parallel HEFT" or
  ;; M-HEFT.
  (cut select-workstation/heft* <> <> <> <> <> <> #t))


;;;
;;; Adhoc multi-core support for SimGrid.
;;;

(define (same-machine? ws1 ws2)
  "Return true if WS2 and WS2 are in fact two cores on the same machine."
  (or (eq? ws1 ws2)
      (let ((m1 (simdag-workstation-property ws1 "machine")))
        (and m1
             (equal? m1 (simdag-workstation-property ws2 "machine"))))))

(define (same-machine-workstations ws workstations)
  "Return the workstations among WORKSTATIONS belonging to the same machine
as WS, including WS."
  (let ((m (simdag-workstation-property ws "machine")))
    (if m
        (filter (lambda (w)
                  (equal? m (simdag-workstation-property w "machine")))
                workstations)
        (list ws))))

(define (workstations->machines+cores ws)
  "Return a list of pairs whose car is a machine name and whose cdr is the
subset of WS belonging to that machine."
  ;; (("MACHINE1" ws1 ws2 ws3) ("MACHINE2" ws4) ...)
  (define (ws-machine ws)
    (or (simdag-workstation-property ws "machine")
        (simdag-workstation-name ws)))

  (fold (lambda (w r)
          (let* ((machine (ws-machine w))
                 (cores   (or (assoc-ref r machine) '())))
            (alist-cons machine (cons w cores)
                        (alist-delete machine r string=?))))
        '()
        ws))


;;;
;;; Task speedup models.
;;;

(define (measured-node-speedup n c)
  "Return the speedup measured for node N on C cores (using `node-speedup'),
or an linear extrapolation based on the available data.  Return #f if the
speedup could not be inferred."
  (let loop ((speedups (sort (filter (negate (compose zero? cdr))
                                     (node-speedup n))
                             (lambda (s1 s2)
                               (< (car s1) (car s2)))))
             (previous #f))
    (if (null? speedups)
        (if previous
            (cdr previous) ; rough approximation
            #f)
        (let ((s (car speedups)))
          (cond ((= (car s) c)
                 ;; Exact match: we have a measure of the speedup for C.
                 (cdr s))
                ((> (car s) c)
                 (let ((c1 (if previous (car previous) 1))
                       (c2 (car s))
                       (s1 (if previous (cdr previous) 1))
                       (s2 (cdr s)))
                   ;; Linear extrapolation, assuming the speedup curve goes
                   ;; from (C1, S1) to (C2, S2), and starts at (1, 1).
                   (+ s1 (* (- c c1)
                            (/ (- s2 s1) (- c2 c1))
                            1.0))))
                (else (loop (cdr speedups) s)))))))

(define (measured-node-speedup* n c)
  "Same as `measured-node-speedup' but fall back to `sublinear-node-speedup'
if no data is available."
  (or (measured-node-speedup n c)
      (sublinear-node-speedup n c)))

(define (sublinear-node-speedup n c)
  "Return a wild guess of the speedup obtained when executing N on C cores."
  (define %configure-time
    ;; Most packages start with `./configure', which takes at least this many
    ;; seconds on hydra.nixos.org machines (data point: GNU Idutils takes
    ;; 111s on these machines.)
    300)

  (define %sequential-proportion
    0.6)


  (define s (node-duration n))

  (if (and s (> s 0))
      ;; Compute P, the execution time on C cores, around the assumption that
      ;; the initial configure phase is always sequential.
      ;; (let ((p (if (> s %configure-time)
      ;;              (+ %configure-time
      ;;                 (/ (- s %configure-time)
      ;;                    (expt c 0.8)))
      ;;              s)))
      (let ((p (if (> s %configure-time)
                   (+ (* s %sequential-proportion)
                      (/ (* s (- 1 %sequential-proportion))
                         c))
                   s)))
        (/ s p))
      1))


;;;
;;; Scheduling engine.
;;;

(define (seconds->flops s)
  ;; Convert an execution time on Hydra into a number of "floating point
  ;; operations".

  ;; The machines behind hydra.nixos.org, where the build times were
  ;; collected, are Intel Xeon E5430 @ 2.66 GHz, which should correspond to
  ;; ~91,500 MIPS.  This figure was obtained by trying to obtain a
  ;; build/transfer time ratio close to the one of the actual machines based
  ;; on the data in `buildsteps'.
  (* 91500e6 s))

(define* (dynamic-scheduling dag workstations
                             #:optional
                             (select-task select-first-task)
                             (select-workstation select-first-workstation)
                             (node-speedup measured-node-speedup*))
  "Dynamic scheduling: sort DAG's nodes topologically; as soon as a
workstation becomes idle, choose a task to schedule using SELECT-TASK and
assign it to the idle workstation chosen by SELECT-WORKSTATION.  If a task is
to be executed in parallel, its speedup is that returned by NODE-SPEEDUP for
the given node and number of cores."

  (define (schedule! task . workstations)
    (simdag-auto-schedule-task task workstations)
    (watch-simdag-task task 'done))

  (define (done? task)
    (eq? (simdag-task-state task) 'done))

  (define (siblings ws)
    (same-machine-workstations ws workstations))

  (define (make-computations n parallelism)
    (let* ((name     (or (node-derivation-path n)
                         (node-output-path n)))
           (seq      (seconds->flops
                      (or (node-duration n) 0)))
           (duration (/ seq (node-speedup n parallelism))))
      (unfold (cut >= <> parallelism)
              (lambda (i)
                (make-simdag-sequential-computation-task
                 (if (> parallelism 1)
                     (format #f "~a||~a" name i)
                     name)
                 duration))
              1+
              0)))

  (define (wrap-parallel-tasks n t+)
    ;; Wrap T+, a list of tasks representing the parallel execution of N, as
    ;; a fork/join.
    (let ((source (make-simdag-sequential-computation-task
                   (string-append (node-output-path n) "||source") 0))
          (sink   (make-simdag-sequential-computation-task
                   (string-append (node-output-path n) "||sink") 0)))
      (for-each (cut add-simdag-task-dependency! <> source) t+)
      (for-each (cut add-simdag-task-dependency! sink <>) t+)
      (values source sink)))

  (define (make-transfer n src dst)
    ;; Make a communication task transferring the result of N from
    ;; workstation SRC to DST.
    (let ((e2e (make-simdag-end-to-end-communication-task
                (string-append (or (node-derivation-path n)
                                   (node-output-path n))
                               (format #f ":~a->~a"
                                       (simdag-workstation-name src)
                                       (simdag-workstation-name dst)))
                (assert (lambda (s)
                          (and (number? s) (> s 0)))
                        (node-size n)))))
      (schedule! e2e src dst)
      e2e))

  (define nodes
    ;; Nodes in the right order, i.e., sources first.
    (phase "topologically sorting nodes"
           (dag-topological-sort dag)))

  (define node-count (length nodes))

  ;; Node sets, like the QUEUED and READY loop variables below, are
  ;; implemented using bignums, which are used as bitvectors.  To that end
  ;; each element of NODES is mapped to its index; if the corresponding bit
  ;; is 1 in a node set, then the node is part of that set.

  (define indices
    (fold vhash-consq vlist-null
          nodes (iota (length nodes))))

  (define (node->index n)
    (cdr (vhash-assq n indices)))

  (define index->node
    (let ((nodes (list->vector nodes)))
      (lambda (i)
        (vector-ref nodes i))))

  (define set-of-all-nodes
    (- (expt 2 node-count) 1))

  (define (nset-empty? set)
    (= 0 set))

  (define (nset-union set nodes)
    (fold (lambda (n s)
            (copy-bit (node->index n) s #t))
          set
          nodes))

  (define nset (cut nset-union 0 <>))

  (define (nset-difference set nodes)
    (fold (lambda (n s)
            (copy-bit (node->index n) s #f))
          set
          nodes))

  (define (nset->nodes set)
    ;; Return the list of nodes corresponding to SET.
    (let loop ((i     0)
               (nodes '()))
      (if (>= i node-count)
          nodes
          (loop (+ 1 i)
                (if (bit-set? i set)
                    (cons (index->node i) nodes)
                    nodes)))))

  (define (augment-cache tasks task->node comm->comp cache)
    ;; Augment the workstation build cache CACHE (i.e., each workstation's
    ;; Nix store) with the result of each of TASKS.  COMM->COMP should map a
    ;; communication task to the corresponding node.
    (fold (lambda (t c)
            (let ((n (or (task->node t)
                         (comm->comp t)))
                  (w (case (simdag-task-kind t)
                       ((sequential-computation)
                        (simdag-task-workstation-ref t 0))
                       ((end-to-end-communication)
                        ;; Choose the destination node.
                        (simdag-task-workstation-ref t 1))
                       (else
                        (error "unknown kind" (simdag-task-kind t))))))

              ;; If N is #f, then it must be a parallel task.
              (if (node? n)
                  (fold (lambda (w c)
                          (vhash-consq w
                                       (vhash-cons (node-output-path n) #t
                                                   (cdr (vhash-assq w c)))
                                       c))
                        c
                        (siblings w))
                  c)))
          cache
          tasks))

  (define (cached? node workstation cache)
    ;; Return true if NODE's result is cached on WORKSTATION.
    (vhash-assoc (node-output-path node)
                 (cdr (vhash-assq workstation cache))))

  (define (needs-transfer? n workstation cache)
    ;; Return true if the result of N needs to be transferred to
    ;; WORKSTATION.
    (let ((size (node-size n)))
      (cond ((cached? n workstation cache) #f)
            ((not size)                    #f)
            (else                          #t))))

  (define (idle-workstations done idle)
    ;; Augment IDLE with any new workstation that became idle as tasks DONE
    ;; ended.

    ;; FIXME: We’d rather use ‘SD_workstation_get_current_task’ to
    ;; determine which workstations are idle but that is not available as
    ;; of SimGrid 3.4.1.
    (delete-duplicates
     (append idle
             (append-map (lambda (t)
                           (case (simdag-task-kind t)
                             ((sequential-computation)
                              (simdag-task-workstations t))
                             (else '())))
                         done))))

  (define (ongoing-transfers ws comms)
    ;; Return a list of communication tasks that involve WS.  COMMS has the
    ;; form: ((COMM-TASK SRC DST) ...).  In fact, there can be several SRC
    ;; and several DST, namely all the workstations that are
    ;; `same-machine-workstations'.
    (filter-map (lambda (t)
                  (and (memq ws t)
                       (car t)))
                comms))

  (with-output-to-file "task-list.txt"
    (lambda ()
      ;; XXX: For debugging purposes.
      (pretty-print (map (lambda (n)
                           (or (node-derivation-path n)
                               (node-output-path n)))
                         nodes))))

  (phase "checking topologically sorted node list"
         ;; XXX: For debugging purposes.
         (or (lset= eq? nodes (dag-nodes dag))
             (begin
               (format #t "sorting failed: ~a~%"
                       (lset-difference eq? (dag-nodes dag) nodes))
               (exit 1))))

  (let loop ((queued (nset-difference set-of-all-nodes
                                      (dag-sources dag)))
             (ready  (nset (dag-sources dag)))
             (idle   workstations)
             (cache  (fold (cut vhash-consq <> vlist-null <>)
                           vlist-null
                           workstations))
             (xfers  vlist-null)
             (comms  '())

             ;; Parallel task sources and sinks.
             (pts        '())

             ;; Mapping from nodes to tasks and vice versa.
             (tasks      vlist-null)
             (back-tasks vlist-null)

             ;; Tasks done.
             (tasks-done '()))

    (define (comm->comp c)
      ;; Return the node associated with communication task C, or #f.
      (and=> (vhash-assq c xfers) cdr))

    (define (node->task n)
      (and=> (vhash-assq n tasks) cdr))

    (define (task->node t)
      (and=> (vhash-assq t back-tasks) cdr))

    (define (dependencies-done? n)
      (let ((deps (force (node-dependencies n))))
        (every (lambda (d)
                 (and=> (node->task d) done?))
               deps)))

    (cond ((and (nset-empty? queued) (nset-empty? ready))
           ;; Finish the simulation and return the list of tasks.
           (let ((left (simdag-simulate -1)))
             (if (null? left)
                 tasks-done
                 (let ((done (filter done? left)))
                   (loop queued
                         ready
                         (idle-workstations (lset-difference eq? done pts) idle)
                         cache
                         xfers
                         (fold alist-delq comms done)
                         (fold delq pts done)
                         tasks back-tasks
                         (append done tasks-done))))))
          ((or (nset-empty? ready) (null? idle))
           ;; Run the simulation to free up some slots.
           (let ((done (filter done? (simdag-simulate -1))))
             (or (null? done)
                 (phase "~{~a ~}" (map simdag-task-name done) #t))
             (if (null? done)
                 (if (nset-empty? ready)
                     (loop queued ready idle cache xfers comms
                           pts tasks back-tasks tasks-done)
                     (begin
                       (format #t
                               "no workstation left to schedule ~A~%"
                               ready)
                       (exit 1)))
                 (let* ((candidates
                         (append-map (lambda (n)
                                       (let ((r (force (node-referrers n))))
                                         (filter dependencies-done? r)))
                                     (filter-map task->node done))))
                   (loop (nset-difference queued candidates)
                         (nset-union
                          (nset-difference ready
                                           (filter-map task->node done))
                          candidates)
                         (idle-workstations (lset-difference eq? done pts) idle)
                         (augment-cache done task->node comm->comp cache)
                         xfers
                         (fold alist-delq comms done)
                         (fold delq pts done)
                         tasks back-tasks
                         (append done tasks-done))))))
          ((pair? idle)
           ;; Schedule a task on an workstation.
           (let*-values (((node remainder)
                          (select-task (nset->nodes ready)))
                         ((ws deps-transfers)
                          (select-workstation idle workstations node
                                              (cut cached? <> <> cache)
                                              (cut ongoing-transfers
                                                   <> comms)
                                              node-speedup)))

             (let* ((ws+   (if (pair? ws) ws (list ws)))
                    (ws    (car ws+)) ;; a representative of WS+
                    (para? (pair? (cdr ws+)))
                    (task+ (make-computations node (length ws+)))
                    (deps  (node-prerequisites node))
                    (xf    (map (lambda (d+w)
                                  (let ((dep (car d+w))
                                        (src (cdr d+w)))
                                    (cons (make-transfer dep src ws)
                                          dep)))
                                deps-transfers)))
               ;; Make sure only idle workstations were selected.
               (assert (every (cut memq <> idle) ws+))

               ;; Make sure they all of WS+ belong to the same machine.
               (assert (every (cut same-machine? ws <>) ws+))

               ;; Make sure all the dependencies of NODE are done.
               (assert (cut every done? <>) (map node->task deps))

               ;; Make sure DEPS-TRANSFERS contains a transfer for every
               ;; item that needs to be transferred to WS.
               (assert (lset= eq?
                              (map car deps-transfers)
                              (filter (cut needs-transfer? <> ws cache)
                                      deps)))

               (let-values (((source sink)
                             (if para?
                                 (wrap-parallel-tasks node task+)
                                 (values (car task+) (car task+)))))
                 (for-each (cut add-simdag-task-dependency! source <>)
                           (map car xf))
                 (if para?
                     (for-each (lambda (t w)
                                 (simdag-auto-schedule-task t (list w)))
                               (cons source task+)
                               (cons ws ws+)))

                 (schedule! sink ws)

                 (loop queued
                       (nset remainder)
                       (fold delq idle ws+)
                       cache
                       (fold (lambda (c+t r)
                               (vhash-consq (car c+t) (cdr c+t) r))
                             xfers
                             xf)
                       (append (map (let ((dst (siblings ws)))
                                      (lambda (c+t d+w)
                                        (let ((src (siblings (cdr d+w))))
                                          (cons (car c+t)
                                                (append src dst)))))
                                    xf
                                    deps-transfers)
                               comms)

                       (if para? (cons* source sink pts) pts)
                       (vhash-consq node sink tasks)
                       (vhash-consq sink node back-tasks)
                       tasks-done)))))
          (else
           (error "unexpected state")))))


;;;
;;; Metrics.
;;;

(define* (critical-path dag #:optional (cores 1))
  "Return a list of nodes from DAG, which is the critical path of DAG,
starting with DAG's sources, when executing it on CORES cores."

  (define (successors n)
    (force (node-referrers n)))

  (define (duration n)
    (let ((d (node-duration n)))
      (if d
          (if (= cores 1)
              d
              (/ d (measured-node-speedup* n cores) 1.0))
          0)))

  (letrec ((path-length
            (lambda (n)
              (force (cdr (vhash-assq n paths)))))
           (paths
            (fold (lambda (n r)
                    (vhash-consq n
                     (delay
                       (+ (duration n)
                          (if (null? (successors n))
                              0
                              (apply max
                                     (map path-length (successors n))))))
                     r))
                  vlist-null
                  (dag-nodes dag))))

    (let loop ((nodes (dag-sources dag))
               (path  '()))
      (if (null? nodes)
          (reverse path)
          (let ((n (highest nodes path-length <)))
            (loop (successors n)
                  (cons n path)))))))

(define (minimum-critical-path-length dag workstations)
  "Return the minimum length of the critical path of DAG, i.e., the time it
takes to compute the critical path on the fastest of WORKSTATIONS without
doing any communications.  This is a lower bound on the optimal schedule
length.

This is similar to `maximum-upward-rank' except that (1) the execution time
on the fastest of WORKSTATIONS is considered instead of the average, and (2)
communication times are ignored."

  (define fastest-workstation
    (highest workstations simdag-workstation-power <))

  (define (successors n)
    (force (node-referrers n)))

  (define (smallest-execution-time n)
    (if (node-duration n)
        (let ((c (seconds->flops (node-duration n))))
          (simdag-workstation-computation-time fastest-workstation c))
        0))


  (letrec ((path-length
            (lambda (n)
              (force (cdr (vhash-assq n paths)))))
           (paths
            (fold (lambda (n r)
                    (vhash-consq n
                     (delay
                       (+ (smallest-execution-time n)
                          (if (null? (successors n))
                              0
                              (apply max
                                     (map path-length (successors n))))))
                     r))
                  vlist-null
                  (dag-nodes dag))))

    (apply max (map path-length (dag-sources dag)))))

(define (minimum-sequential-computation-time dag workstations)
  "Return the time it would take to execute DAG sequentially on the fastest
of WORKSTATIONS."
  (define fastest
    (highest workstations simdag-workstation-power <))

  (fold (lambda (n t)
          (if (node-duration n)
              (let ((c (seconds->flops (node-duration n))))
                (+ (simdag-workstation-computation-time fastest c) t))
              t))
        0.0
        (dag-nodes dag)))
