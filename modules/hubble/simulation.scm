;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; -*- coding: utf-8 -*-
;;; Copyright (C) 2010 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (hubble simulation)
  #:use-module (simgrid)
  #:use-module (hubble utils)
  #:use-module (hubble scheduling)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (sxml simple)
  #:use-module (ice-9 match)
  #:export (simulate

            total-duration
            total-task-time
            total-transfer-time
            expected-transfer-time
            transfer-slowdowns
            transfer-average-stretch
            total-computation-time
            workstation-computation-time
            data-transferred))


;;;
;;; Running a simulation.
;;;

(define (simulate dag platform make-algorithms)
  "Simulate DAG on PLATFORM (an SXML platform description) using
MAKE-ALGORITHM to instantiate the task and workstation selection algorithms.
Return an alist that synthesizes the simulation."
  (define (mkstemp template)
    (let ((x (string-copy template)))
      (mkstemp! x)
      x))

  (let ((xml-file (mkstemp "/tmp/hubble-XXXXXX")))
    (dynamic-wind
      (lambda ()
        (simdag-init! '()))
      (lambda ()
        (let ((out (open-output-file xml-file)))
          (sxml->xml platform out)
          (close-port out))

        (make-simdag-environment xml-file)
        (let* ((workstations (simdag-workstations))
               (algorithms   (make-algorithms dag workstations))
               (tasks        (apply dynamic-scheduling dag workstations
                                    algorithms)))
          ;; Synthesize the result because the workstation and task objects
          ;; can't be accessed after `simdag-exit!' (presumably.)
          (synthesize-simulation dag tasks workstations)))
      (lambda ()
        (simdag-exit!)
        (false-if-exception (delete-file xml-file))))))

(define (synthesize-simulation dag tasks workstations)
  "Return an alist that summarizes the execution of TASKS on WORKSTATIONS.
The alist doesn't contain any SimDAG object (because SimDAG objects die when
`simdag-exit!' is called) and is serializable."
  (let ((pairs (filter (lambda (w+w)
                         (not (same-machine? (first w+w) (second w+w))))
                       (cartesian-product (list workstations workstations)))))
    `((workstations      . ,(map simdag-workstation-name workstations))
      (workstation-power . ,(map simdag-workstation-power workstations))
      (bandwidth         . ,(map (lambda (w+w)
                                   (simdag-communication-bandwidth (first w+w)
                                                                   (second w+w)))
                                 pairs))
      (latency           . ,(map (lambda (w+w)
                                   (simdag-communication-latency (first w+w)
                                                                 (second w+w)))
                                 pairs))
      (tasks-run         . ,(length tasks))
      (total-duration    . ,(total-duration tasks))
      (total-transfer-time . ,(total-transfer-time tasks))
      (expected-transfer-time . ,(expected-transfer-time tasks))
      (transfer-slowdowns . ,(transfer-slowdowns tasks))
      (total-computation-time . ,(total-computation-time tasks))
      (workstation-computation-time
       . ,(let-values (((ws time)
                        (unzip2 (workstation-computation-time workstations
                                                              tasks))))
            (zip (map simdag-workstation-name ws) time)))
      (minimum-critical-path-length
       . ,(minimum-critical-path-length dag workstations))
      (minimum-sequential-computation-time
       . ,(minimum-sequential-computation-time dag workstations))
      (data-transferred  . ,(data-transferred tasks)))))


;;;
;;; Statistics.
;;;

(define (total-duration tasks)
  "Return the total duration of executing TASKS, i.e., the time at which the
last task finished.  This is also referred to as the \"schedule length\"."
  (fold (lambda (t e)
          (let ((t (simdag-task-finish-time t)))
            (if (> t e)
                t
                e)))
        0.0
        tasks))

(define (total-task-time tasks)
  "Return the sum of the duration of each of TASKS."
  (fold (lambda (t d)
          (+ (- (simdag-task-finish-time t)
                (simdag-task-start-time t))
             d))
        0.0
        tasks))

(define (total-transfer-time tasks)
  "Return the sum of the durations of the data transfer tasks among TASKS."
  (total-task-time (filter (lambda (t)
                             (eq? (simdag-task-kind t)
                                  'end-to-end-communication))
                           tasks)))

(define (expected-transfer-time tasks)
  "Return the \"expected\" transfer time for all communication tasks among
TASKS.  Here \"expected\" means \"if there were no contention.\""
  (fold (lambda (t e)
          (if (eq? (simdag-task-kind t) 'end-to-end-communication)
              (match (simdag-task-workstations t)
                ((src dst)
                 (let ((amount (simdag-task-amount t)))
                   (+ e (simdag-communication-time src dst amount))))
                (_ (error "wrong communication task" t)))
              e))
        0.0
        tasks))

(define (transfer-slowdowns tasks)
  "Return the list of transfer slowdowns for each communication task among
TASKS."
  (fold-right (lambda (t r)
                (if (eq? (simdag-task-kind t) 'end-to-end-communication)
                    (match (simdag-task-workstations t)
                      ((src dst)
                       (let* ((amount (simdag-task-amount t))
                              (time   (- (simdag-task-finish-time t)
                                         (simdag-task-start-time t)))
                              (ideal  (simdag-communication-time src dst
                                                                 amount)))
                         (cons (/ time ideal) r))))
                    r))
              '()
              tasks))

(define (transfer-average-stretch tasks)
  "Return the average stretch for data transfers among TASKS, i.e., the
average slowdown factor in transfers; the lower,the better.  A high stretch
means that there was a lot of network contention."
  (average (transfer-slowdowns tasks)))

(define (total-computation-time tasks)
  "Return the sum of the durations of the computation tasks among TASKS."
  (total-task-time (filter (lambda (t)
                             (eq? (simdag-task-kind t)
                                  'sequential-computation))
                           tasks)))

(define (workstation-computation-time workstations tasks)
  "Return a list of (WORKSTATION TIME) pairs, that shows the computation time
spent by each WORKSTATION while evaluating TASKS."
  (define (update-workstation-stats task ws busy-time)
    (case (simdag-task-kind task)
      ((sequential-computation)
       (if (memq ws (simdag-task-workstations task))
           (let ((duration (- (simdag-task-finish-time task)
                              (simdag-task-start-time task))))
             (+ duration busy-time))
           busy-time))
      (else busy-time)))

  (zip workstations
       (fold (lambda (t r)
               (if (eq? (simdag-task-state t) 'done)
                   (map (cut update-workstation-stats t <> <>)
                        workstations
                        r)
                   r))
             (circular-list 0.0)
             tasks)))

(define (data-transferred tasks)
  "Return the amount of data (in bytes) transferred to fulfill the execution
of TASKS."
  (fold (lambda (t x)
          (if (eq? (simdag-task-kind t)
                   'end-to-end-communication)
              (+ x (simdag-task-amount t))
              x))
        0
        tasks))
