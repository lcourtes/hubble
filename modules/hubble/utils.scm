;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010, 2011, 2012 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (hubble utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 format)
  #:use-module (ice-9 regex)
  #:export (phase
            assert

            derivation-path?
            newer?
            strip-store-path

            seconds->string
            cartesian-product
            random-ref
            highest
            highest*
            highest/randomized
            average
            standard-deviation
            exponential?
            alist-delq

            with-temporary-file))


;;;
;;; Macros.
;;;

(define %beginning
  ;; The time when it all began.
  (car (gettimeofday)))

(define-syntax phase
  ;; Write to stderr what's going on.
  (lambda (s)
    (syntax-case s ()
      ((_ fmt args ... body)
       (string? (syntax->datum #'fmt))
       (with-syntax ((fmt*    (string-append "[~4d] "
                                             (syntax->datum #'fmt)
                                             "..."))
                     (elapsed #'(- (car (gettimeofday)) %beginning)))
         #`(begin
             (format (current-error-port) fmt* elapsed args ...)
             (let ((result (call-with-values (lambda () body) list)))
               (format (current-error-port) "  done~%")
               (apply values result))))))))

(define-syntax assert
  (lambda (s)
    (syntax-case s ()
      ((_ exp)
       (with-syntax ((msg (format #f "assertion `~S' failed"
                                  (syntax->datum #'exp))))
         #`(let ((result exp) (loc (current-source-location)))
             (if (not result)
                 (throw 'assertion-failure msg loc)
                 result))))
      ((_ pred exp)
       (with-syntax ((fmt (format #f "assertion `~S' failed: ~~S"
                                  (syntax->datum #'exp))))
         #`(let ((result exp) (loc (current-source-location)))
             (if (not (pred result))
                 (throw 'assertion-failure (format #f fmt result) loc)
                 result)))))))


;;;
;;; Nix-releated tools
;;;

(define (newer? file1 file2)
  ;; Return #t if FILE1 is newer than FILE2.
  (let ((s1 (stat file1))
        (s2 (stat file2)))
    (> (stat:mtime s1) (stat:mtime s2))))

(define (derivation-path? path)
  "Return true if PATH is a derivation path."
  (string-suffix? ".drv" path))

(define strip-store-path
  (let ((store-path-rx
         (make-regexp "^.*/nix/store/[^-]+-(.+)$")))
    (lambda (path)
      (or (and=> (regexp-exec store-path-rx path)
                 (lambda (match)
                   (let ((path (match:substring match 1)))
                     path)))
          path))))

;;;
;;; Various.
;;;

(define (seconds->string seconds)
  (and seconds
       (let ((seconds (inexact->exact (round seconds))))
         (let ((s (modulo seconds 60))
               (m (modulo (quotient seconds 60) 60))
               (h (quotient seconds 3600)))
           ;; Note: we use non-breaking spaces between
           ;; hours/minutes/seconds to improve layout.
           (format #f "~:[~ah ~;~*~]~:[~am ~;~*~]~as"
                   (zero? h) h
                   (zero? m) m
                   s)))))

(define (cartesian-product lists)
  "Return the cartesian product of all the SETS."
  ;; Shamelessly stolen from
  ;; <http://mumble.net/~campbell/scheme/loop-comparison.scm>, which is in
  ;; the public domain.
  ;; XXX: We use `(fold f s (reverse l))' instead of `(fold-right f s l)'
  ;; because the implentation of `fold-right' in 1.9.12 isn't
  ;; tail-recursive.
  (fold (lambda (list tails)
          (fold (lambda (item product)
                  (fold (lambda (tail product)
                          (cons (cons item tail)
                                product))
                        product
                        (reverse tails)))
                '()
                (reverse list)))
        '(())
        (reverse lists)))

(define random-ref
  (let ((state (seed->random-state (+ (car (gettimeofday))
                                      (cdr (gettimeofday))))))
    (lambda (lst)
      "Return an element randomly chosen from LST."
      (list-ref lst (random (length lst) state)))))

(define (highest lst decorate <)
  "Return the item of LST that is a maximum according to the < relation.
DECORATE is used to compute the value associated with an item of LST."
  (if (null? lst)
      #f
      (let ((worth (map decorate lst)))
        (let loop ((lst       lst)
                   (worth     worth)
                   (candidate (car lst))
                   (max-value (car worth)))
          (if (null? lst)
              candidate
              (if (< (car worth) max-value)
                  (loop (cdr lst) (cdr worth)
                        candidate max-value)
                  (loop (cdr lst) (cdr worth)
                        (car lst) (car worth))))))))

(define (highest* lst decorate <)
  "Return all the items of LST that are a maximum according to the < relation.
DECORATE is used to compute the value associated with an item of LST."
  (if (null? lst)
      '()
      (let ((worth (map decorate lst)))
        (let loop ((lst        lst)
                   (worth      worth)
                   (candidates '())
                   (max-value  (car worth)))
          (if (null? lst)
              (reverse candidates)
              (let ((value (car worth)))
                (cond ((< value max-value)
                       (loop (cdr lst) (cdr worth)
                             candidates max-value))
                      ((equal? value max-value)
                       (loop (cdr lst) (cdr worth)
                             (cons (car lst) candidates)
                             max-value))
                      (else
                       (loop (cdr lst) (cdr worth)
                             (list (car lst)) value)))))))))

(define (highest/randomized lst decorate <)
  "Like `highest', except that when several elements of `(map DECORATE LST)'
are `equal?', then a random one is returned."
  (if (null? lst)
      #f
      (random-ref (highest* lst decorate <))))

(define (average lst)
  "Return the average of the numbers in LST."
  (if (null? lst)
      0
      (/ (reduce + 0 lst) (length lst))))

(define (standard-deviation lst)
  "Return the standard deviation of the numbers in LST."
  (define (pow2 x)
    ;; Don't use `expt' to work around Guile bug #31464
    ;; <https://savannah.gnu.org/bugs/index.php?31464>.
    (* x x))

  (let ((a (average lst)))
    (sqrt (average (map (cut pow2 <>)
                        (map (cut - <> a) lst))))))

(define (exponential? lst)
  "Return #t if the numbers in LST form an exponential sequence."
  (let ((lst (remove zero? lst)))
    (or (null? lst)
        (null? (cdr lst))
        (let* ((ratio     (average (map / lst (cdr lst))))
               (tolerance (* 1/10 ratio)))
          (every (lambda (a b)
                   (<= (abs (- (/ a b) ratio)) tolerance))
                 lst
                 (cdr lst))))))

(define alist-delq (cut alist-delete <> <> eq?))

(define (mkstemp template)
  (let ((x (string-copy template)))
    (mkstemp! x)
    x))

(define-syntax with-temporary-file
  (syntax-rules ()
    "Call THUNK with a fresh temporary file name."
    ((_ thunk)
     (let ((file-name (mkstemp "hubble-XXXXXX")))
       (dynamic-wind
         (lambda () #t)
         (lambda ()
           (thunk file-name))
         (lambda ()
           (false-if-exception (delete-file file-name))))))))
