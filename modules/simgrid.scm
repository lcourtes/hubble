;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010, 2011, 2012 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (simgrid)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 format)
  #:export (simdag-init!
            simdag-exit!
            make-simdag-environment
            make-simdag-task
            simdag-task?
            make-simdag-sequential-computation-task
            make-simdag-end-to-end-communication-task
            simdag-task-name
            simdag-task-amount
            simdag-task-kind
            simdag-task-state
            simdag-task-start-time
            simdag-task-finish-time
            simdag-task-workstations
            simdag-task-workstation-ref
            add-simdag-task-dependency!
            simdag-task-dependency-exists?
            dump-simdag-task
            watch-simdag-task
            simdag-task-execution-time
            simdag-schedule-task
            simdag-auto-schedule-task
            simdag-simulate
            simdag-workstation?
            simdag-workstations
            simdag-workstation-name
            simdag-workstation-power
            simdag-workstation-available-power
            simdag-workstation-property
            simdag-workstation-computation-time
            simdag-communication-time
            simdag-communication-latency
            simdag-communication-bandwidth
            ;; simdag-workstation-current-task
            simdag-load-dax
            ))

(define %libsimgrid
  (catch 'misc-error
    (lambda ()
      (dynamic-link "libsimgrid"))
    (lambda (key subr message args . rest)
      (apply format (current-error-port) message args)
      (newline (current-error-port))
      (format (current-error-port)
              "Please make sure `libsimgrid.so' is in $LD_LIBRARY_PATH or similar.~%")
      (exit 1))))


;;;
;;; Helpers.
;;;

(define (foreign-array->list array-pointer element-count)
  (let ((array (pointer->bytevector array-pointer
                                    (* element-count (sizeof '*)))))
    (unfold (cut >= <> element-count)
            (lambda (element)
              (let ((start (* element (sizeof '*))))
                (bytevector->pointer array start)))
            1+
            0)))

(define (align off alignment)
  ;; Stolen from (system foreign).
  (1+ (logior (1- off) (1- alignment))))

(define (pointer+ array-pointer type index)
  ;; XXX: Should be a (system foreign) primitive.
  ;; FIXME: Turn it into a macro so that `sizeof', `alignof' & co. are
  ;; computed at compile time.
  (let ((offset (* index (align (sizeof type) (alignof type)))))
    (make-pointer (+ (pointer-address array-pointer) offset))))

(define (make-pointer-to-int value)
  ;; Return a foreign pointer to an `int' equal to VALUE.
  (let ((bv (make-bytevector (sizeof int))))
    (bytevector-uint-set! bv 0 value (native-endianness)
                          (sizeof int))
    (bytevector->pointer bv)))

(define (make-pointer-to-pointer ptr)
  "Box PTR, as in `&ptr'."
  (let ((bv (make-bytevector (sizeof '*))))
    (bytevector-uint-set! bv 0 (pointer-address ptr) (native-endianness)
                          (sizeof '*))
    (bytevector->pointer bv)))

(define (dereference-pointer-to-int ptr)
  ;; Return the `int' value pointed to by PTR.
  (let ((bv (pointer->bytevector ptr (sizeof int))))
    (bytevector-uint-ref bv 0 (native-endianness) (sizeof int))))

(define (list->foreign-string-array lst)
  ;; Return a pointer to a foreign string array containing the strings listed
  ;; in LST.
  (let* ((count    (length lst))
         (array    (make-bytevector (* count (sizeof '*))))
         (fstrings (map string->pointer lst)))
    (fold (lambda (fstr index)
            (bytevector-uint-set! array index (pointer-address fstr)
                                  (native-endianness)
                                  (sizeof '*))
            (+ index (sizeof '*)))
          0
          fstrings)
    (bytevector->pointer array)))

(define (foreign-string-array->list array len)
  ;; Return a list of string comprising the LEN strings pointed to by the
  ;; elements of ARRAY, a pointer to an array of pointers.
  (unfold (cut < <> 0)
          (lambda (index)
            (let ((ptr (make-pointer (+ (pointer-address array)
                                        (* index (sizeof '*))))))
             (pointer->string (dereference-pointer ptr))))
          1-
          (- len 1)))

(define (simgrid-func name)
  (dynamic-func name %libsimgrid))

(define-syntax define-simgrid-pointer-type
  (lambda (stx)
    (syntax-case stx ()
      ((_ type-name)
       (let* ((type-name*  (syntax->datum #'type-name))
              (pred-name   (datum->syntax #'type-name
                                          (symbol-append type-name* '?)))
              (wrap-name   (datum->syntax #'type-name
                                          (symbol-append 'wrap- type-name*)))
              (%wrap-name  (datum->syntax #'type-name
                                          (symbol-append '%wrap- type-name*)))
              (unwrap-name (datum->syntax #'type-name
                                          (symbol-append 'unwrap-
                                                         type-name*))))
         (with-syntax ((pred   pred-name)
                       (wrap   wrap-name)
                       (%wrap  %wrap-name)
                       (unwrap unwrap-name))
           #'(begin
               (define-record-type type-name
                 (%wrap pointer)
                 pred
                 (pointer unwrap))
               (define wrap
                 ;; Use a weak hash table to preserve pointer identity, i.e.,
                 ;; PTR1 == PTR2 <-> (eq? (wrap PTR1) (wrap PTR2)).
                 (let ((ptr->obj (make-weak-value-hash-table 3000)))
                   (lambda (ptr)
                     (let ((key+value (hash-create-handle! ptr->obj ptr #f)))
                       (or (cdr key+value)
                           (let ((o (%wrap ptr)))
                             (set-cdr! key+value o)
                             o)))))))))))))

(define-syntax define-simgrid-pointer-type-accessor
  (lambda (stx)
    (syntax-case stx ()
      ((_ name type-name slot-type c-func)
       #'(define-simgrid-pointer-type-accessor name type-name
           slot-type c-func identity))
      ((_ name type-name slot-type c-func wrap-result)
       (let* ((type-name*  (syntax->datum #'type-name))
              (unwrap-name (datum->syntax #'type-name
                                          (symbol-append 'unwrap-
                                                         type-name*))))
         (with-syntax ((unwrap unwrap-name))
           #'(define name
               (let ((f (pointer->procedure slot-type
                                            (simgrid-func c-func)
                                            '(*))))
                 (lambda (obj)
                   (wrap-result (f (unwrap obj))))))))))))


;;;
;;; XBT, SimGrid's extra-cool base toolkit(TM).
;;;

(define (compose proc . rest)
  ;; FIXME: Should be in Guile core, along with others from
  ;; <http://docs.racket-lang.org/reference/procedures.html#%28def._%28%28lib._racket/private/base..rkt%29._compose%29%29>.
  "Compose PROC with the procedures in REST, applying the last one in REST
first and PROC last, and return the resulting function.  The given procedures
must have compatible arity."
  (if (null? rest)
      proc
      (let ((g (apply compose rest)))
        (lambda args
          (call-with-values (lambda () (apply g args)) proc)))))

(define dynar-free
  (let ((f (pointer->procedure void
                               (simgrid-func "xbt_dynar_free")
                               '(*))))
    (lambda (ptr)
      "Free the dynar pointed to by PTR."
      (f (make-pointer-to-pointer ptr)))))

(define dynar-length
  (pointer->procedure unsigned-long
                      (simgrid-func "xbt_dynar_length")
                      '(*)))

(define dynar->list
  (let* ((ref! (pointer->procedure void
                                  (simgrid-func "xbt_dynar_get_cpy")
                                  `(* ,unsigned-long *)))
         (ref  (lambda (ptr i ptrloc)
                 (ref! ptr i ptrloc)
                 (dereference-pointer ptrloc))))
    (lambda (ptr wrap)
      "Return a list of pointer objects for the dynar pointed to by PTR."
      (let ((len    (dynar-length ptr))
            (ptrloc (bytevector->pointer (make-bytevector (sizeof '*)))))
        (unfold (cut >= <> len)
                (cute (compose wrap (cut ref <> <> ptrloc)) ptr <>)
                1+
                0)))))


;;;
;;; SimDAG.
;;;

(define simdag-init!
  (let ((f (pointer->procedure void
                               (simgrid-func "SD_init")
                               '(* *))))
    (lambda (args)
      "Initialize SimGrid with argument ARGS.  This procedure *must* be
called by users before anything else!"
      (let* ((args (cons (car (program-arguments)) args))
             (argc (make-pointer-to-int (length args)))
             (argv (list->foreign-string-array args)))
       (f argc argv)
       (foreign-string-array->list argv
                                   (dereference-pointer-to-int argc))))))

(define make-simdag-environment
  (let ((f (pointer->procedure void
                               (simgrid-func "SD_create_environment")
                               '(*))))
    (lambda (platform-file)
      (f (string->pointer platform-file)))))

;;;
;;; Task state.
;;;

;; From <simdag/datatypes.h>.

(define %states
  '((not-scheduled . 0)
    (scheduled . #x0001)
    (ready . #x0002)
    (runnable . #x0004) ;; new in SimGrid 3.5
    (in-fifo . #x0008)
    (running . #x0010)
    (done . #x0020)
    (failed . #x0040)))

(define (int->state i)
  (or (any (lambda (x)
             (and (= i (cdr x))
                  (car x)))
           %states)
      i))

(define (state->int s)
  (or (assoc-ref %states s)
      (error "unknown state" s)))

(define (int->kind i)
  (case i
    ((0) #f)
    ((1) 'end-to-end-communication)
    ((2) 'sequential-computation)
    (else i)))

;;;
;;; Tasks.
;;;

;; The list of live task objects for the current simulation.  We keep track
;; of them so that they are destroyed exactly when the simulation is exited.
(define %live-tasks '())

(define-wrapped-pointer-type simdag-task
  simdag-task?
  wrap-simdag-task unwrap-simdag-task
  (lambda (task port)
    (format port "#<task ~x ~A ~S ~A>"
            (pointer-address (unwrap-simdag-task task))
            (simdag-task-kind task)
            (simdag-task-name task)
            (simdag-task-state task))))


(define destroy-simdag-task!
  (pointer->procedure void (simgrid-func "SD_task_destroy") '(*)))

(define simdag-exit!
  (let ((f (pointer->procedure void
                               (simgrid-func "SD_exit")
                               '())))
    (lambda ()
      ;; Now is a good time to destroy all the tasks associated with the
      ;; current simulation.
      ;; FIXME: Invalidate the wrappers so that they don't contain dangling
      ;; pointers.
      (for-each destroy-simdag-task!
                (map unwrap-simdag-task %live-tasks))
      (set! %live-tasks '())
      (f))))

(define make-simdag-task
  (let ((f (pointer->procedure '*
                               (simgrid-func "SD_task_create")
                               (list '* '* double))))
    (lambda (name amount)
      (let* ((ptr  (f (string->pointer name) %null-pointer amount))
             (task (wrap-simdag-task ptr)))
        (set! %live-tasks (cons task %live-tasks))
        task))))

(define make-simdag-sequential-computation-task
  (let ((f (pointer->procedure '*
                               (simgrid-func "SD_task_create_comp_seq")
                               (list '* '* double))))
    (lambda (name amount)
      (let* ((ptr  (f (string->pointer name) %null-pointer amount))
             (task (wrap-simdag-task ptr)))
        (set! %live-tasks (cons task %live-tasks))
        task))))

(define make-simdag-end-to-end-communication-task
  (let ((f (pointer->procedure '*
                               (simgrid-func "SD_task_create_comm_e2e")
                               (list '* '* double))))
    (lambda (name amount)
      (let* ((ptr  (f (string->pointer name) %null-pointer amount))
             (task (wrap-simdag-task ptr)))
        (set! %live-tasks (cons task %live-tasks))
        task))))

(define-simgrid-pointer-type-accessor simdag-task-name
  simdag-task '* "SD_task_get_name" pointer->string)

(define-simgrid-pointer-type-accessor simdag-task-amount
  simdag-task double "SD_task_get_amount")

(define-simgrid-pointer-type-accessor simdag-task-kind
  simdag-task int "SD_task_get_kind" int->kind)

(define-simgrid-pointer-type-accessor simdag-task-state
  simdag-task int "SD_task_get_state" int->state)

(define-simgrid-pointer-type-accessor simdag-task-start-time
  simdag-task double "SD_task_get_start_time")

(define-simgrid-pointer-type-accessor simdag-task-finish-time
  simdag-task double "SD_task_get_finish_time")

(define-simgrid-pointer-type-accessor dump-simdag-task
  simdag-task void "SD_task_dump")

(define simdag-simulate
  (let ((f (pointer->procedure '*
                               (simgrid-func "SD_simulate")
                               (list double))))
    (lambda (how-long)
      "Run the simulation for HOW-LONG time units and return the list of
tasks changed.  If HOW-LONG is -1, run until there's nothing left to do."

      ;; Note: In SimGrid 3.4.1, `SD_simulate' used to return a
      ;; null-terminated C array; in 3.5, it returns a dynar.
      (let* ((ptr    (f how-long))
             (result (dynar->list ptr wrap-simdag-task)))
        (dynar-free ptr)
        result))))

(define watch-simdag-task
  (let ((f (pointer->procedure void
                               (simgrid-func "SD_task_watch")
                               (list '* int))))
    (lambda (task state)
      (f (unwrap-simdag-task task) (state->int state)))))

;;;
;;; Dependencies.
;;;

(define add-simdag-task-dependency!
  (let ((f (pointer->procedure void
                               (simgrid-func "SD_task_dependency_add")
                               '(* * * *))))
    (lambda* (task dep #:optional name)
      ;; Make TASK depend on DEP.  Warning: The order of arguments is
      ;; reversed compared to `SD_task_dependency_add'!
      (f (if name
             (string->pointer name)
             %null-pointer)
         %null-pointer
         (unwrap-simdag-task dep)
         (unwrap-simdag-task task)))))

(define simdag-task-dependency-exists?
  (let ((f (pointer->procedure int
                               (simgrid-func "SD_task_dependency_exists")
                               '(* *))))
    (lambda (task maybe-dep)
      ;; Return true if TASK depends on MAYBE-DEP.  Warning: The order of
      ;; arguments is reversed compared to `SD_task_dependency_exists'!
      (not (= 0 (f (unwrap-simdag-task maybe-dep) (unwrap-simdag-task task)))))))

;;;
;;; Workstations.
;;;

(define-wrapped-pointer-type simdag-workstation
  simdag-workstation?
  wrap-simdag-workstation unwrap-simdag-workstation
  (lambda (workstation port)
    (format port "#<workstation ~x ~S>"
            (pointer-address
             (unwrap-simdag-workstation workstation))
            (simdag-workstation-name workstation))))

(define simdag-workstations
  (let ((ws-number (pointer->procedure int
                                       (simgrid-func
                                        "SD_workstation_get_number")
                                       '()))
        (ws-list   (pointer->procedure '*
                                       (simgrid-func
                                        "SD_workstation_get_list")
                                       '())))
    (lambda ()
      (map (lambda (p)
             (wrap-simdag-workstation (dereference-pointer p)))
           (foreign-array->list (ws-list) (ws-number))))))


(define-simgrid-pointer-type-accessor simdag-workstation-name
  simdag-workstation '* "SD_workstation_get_name" pointer->string)

(define-simgrid-pointer-type-accessor simdag-workstation-power
  simdag-workstation double "SD_workstation_get_power")

(define-simgrid-pointer-type-accessor simdag-workstation-available-power
  simdag-workstation double "SD_workstation_get_available_power")

(define simdag-workstation-property
  (let ((f (pointer->procedure '*
                               (simgrid-func
                                "SD_workstation_get_property_value")
                               '(* *))))
    (lambda (ws prop)
      "Return the value of property PROP of workstation WS, a string, or #f
if no such property exists."
      (let ((p (f (unwrap-simdag-workstation ws) (string->pointer prop))))
        (if (null-pointer? p)
            #f
            (pointer->string p))))))

(define simdag-workstation-computation-time
  (let ((f (pointer->procedure double
                               (simgrid-func
                                "SD_workstation_get_computation_time")
                               (list '* double))))
    (lambda (ws amount)
      "Return the time needed to compute AMOUNT flops on workstation WS."
      (f (unwrap-simdag-workstation ws) amount))))

(define simdag-communication-latency
  (let ((f (pointer->procedure double
                               (simgrid-func
                                "SD_route_get_current_latency")
                               '(* *))))
    (lambda (src dst)
      "Return the latency of the route between SRC and DST, i.e., the sum of
all link latencies between them."
      (f (unwrap-simdag-workstation src) (unwrap-simdag-workstation dst)))))

(define simdag-communication-bandwidth
  (let ((f (pointer->procedure double
                               (simgrid-func
                                "SD_route_get_current_bandwidth")
                               '(* *))))
    (lambda (src dst)
      "Return the bandwidth of the route between SRC and DST, i.e., the
minimum bandwidth of all links between them."
      (f (unwrap-simdag-workstation src) (unwrap-simdag-workstation dst)))))

(define simdag-communication-time
  (let ((f (pointer->procedure double
                               (simgrid-func
                                "SD_route_get_communication_time")
                               (list '* '* double))))
    (lambda (src dst amount)
      "Return the estimated time it takes to transfer AMOUNT data from
workstation SRC to DST."
      (f (unwrap-simdag-workstation src) (unwrap-simdag-workstation dst)
         amount))))

;; FIXME: This function is documented but unavailable as of SimGrid 3.4.1.
;; (define-simgrid-pointer-type-accessor simdag-workstation-current-task
;;   simdag-workstation '* "SD_workstation_get_current_task" wrap-simdag-task)


;;;
;;; Scheduling.
;;;

(define (workstations->foreign-array workstations)
  ;; Take a list of workstations and return length of WORKSTATIONS, and a C
  ;; array of workstation pointers.
  (let* ((count      (length workstations))
         (ws-array   (make-bytevector (* count (sizeof '*)))))
    (fold (lambda (ws index)
            (let ((ptr (pointer-address (unwrap-simdag-workstation ws))))
              (bytevector-uint-set! ws-array index ptr (native-endianness)
                                    (sizeof '*))
              (+ index (sizeof '*))))
          0
          workstations)

    (values count
            (bytevector->pointer ws-array))))

(define (workstation+computation+communication->foreign-arrays workstations)
  ;; Take a list of (WORKSTATION COMPUTATION-AMOUNT COMMUNICATION-AMOUNT)
  ;; tuples and return 4 values: the length of WORKSTATIONS, and 3 C arrays
  ;; corresponding to each piece of data.
  (let* ((count      (length workstations))
         (ws-array   (make-bytevector (* count (sizeof '*))))
         (comp-array (make-bytevector (* count (sizeof double))))
         (comm-array (make-bytevector (* count (sizeof double)))))
    (fold (lambda (ws index)
            (let ((ptr (pointer-address (unwrap-simdag-workstation ws))))
              (bytevector-uint-set! ws-array index ptr (native-endianness)
                                    (sizeof '*))
              (+ index (sizeof '*))))
          0
          (map first workstations))
    (fold (lambda (tuple index)
            (let ((comp (second tuple))
                  (comm (third tuple)))
              (bytevector-ieee-double-native-set! comp-array index comp)
              (bytevector-ieee-double-native-set! comm-array index comm)
              (+ index (sizeof double))))
          0
          workstations)

    (values count
            (bytevector->pointer ws-array)
            (bytevector->pointer comp-array)
            (bytevector->pointer comm-array))))

(define simdag-task-execution-time
  (let ((f (pointer->procedure double
                               (simgrid-func "SD_task_get_execution_time")
                               (list '* int '* '* '*))))
    (lambda (task workstations)
      (let-values (((count ws-array comp-array comm-array)
                    (workstation+computation+communication->foreign-arrays
                     workstations)))
        (f (unwrap-simdag-task task)
           count ws-array comp-array comm-array)))))

(define simdag-schedule-task
  (let ((f (pointer->procedure void
                               (simgrid-func "SD_task_schedule")
                               (list '* int '* '* '* double))))
    (lambda (task workstations rate)
      (let-values (((count ws-array comp-array comm-array)
                    (workstation+computation+communication->foreign-arrays
                     workstations)))
        (f (unwrap-simdag-task task)
           count ws-array comp-array comm-array
           rate)))))

(define simdag-auto-schedule-task
  (let ((f (pointer->procedure void
                               (simgrid-func "SD_task_schedulev")
                               (list '* int '*))))
    (lambda (task workstations)
      (let-values (((count ws-array)
                    (workstations->foreign-array workstations)))
        (f (unwrap-simdag-task task)
           count ws-array)))))

(define %task-workstation-count
  (pointer->procedure int
                      (simgrid-func
                       "SD_task_get_workstation_count")
                      '(*)))

(define %task-workstation-list
  (pointer->procedure '*
                      (simgrid-func
                       "SD_task_get_workstation_list")
                      '(*)))

(define (simdag-task-workstations task)
  "Return the list of workstation(s) TASK was assigned to."
  (let ((ptr (unwrap-simdag-task task)))
    (map (lambda (p)
           (wrap-simdag-workstation (dereference-pointer p)))
         (foreign-array->list (%task-workstation-list ptr)
                              (%task-workstation-count ptr)))))

(define (simdag-task-workstation-ref task index)
  "Return the INDEXth workstation TASK was assigned to."
  (let ((ptr (unwrap-simdag-task task)))
    (if (and (>= index 0) (< index (%task-workstation-count ptr)))
        (let ((array (%task-workstation-list ptr)))
          (wrap-simdag-workstation
           (dereference-pointer (pointer+ array '* index))))
        (error 'out-of-range "simdag-task-workstation-ref" index))))

(define simdag-load-dax
  (let ((f (pointer->procedure '* (simgrid-func "SD_daxload")
                               '(*))))
    (lambda (file)
      "Load the DAX file FILE and return the list of SimDAG tasks created."
      (dynar->list (f (string->pointer file)) wrap-simdag-task))))
