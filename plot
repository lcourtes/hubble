#!/bin/sh
# -*- mode: scheme; coding: utf-8; -*-
main='(@ (plot) plot)'
exec ${GUILE-guile} -L "$PWD/modules" -l "$0"    \
         -c "(apply $main (cdr (command-line)))" "$@"
!#
;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (plot)
  #:use-module (hubble output)
  #:use-module (hubble utils)
  #:use-module (hubble dag)
  #:use-module (hubble platform)
  #:use-module (hubble simulation)
  #:use-module (hubble scheduling)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-37)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:export (plot))


;;;
;;; Utils.
;;;

(define (workstation-names->machines+cores names)
  "Assuming NAMES is a list of workstation names that follow the convention
`MACHINE-NAME/CORE-NAME', return an alist of the form
`((MACHINE-NAME CORE-NAME ..) ...)'.  Yes, this is a hack (XXX)."
  (fold (lambda (w r)
          (let* ((machine (first w))
                 (cores   (or (assoc-ref r machine) '())))
            (alist-cons machine (cons (second w) cores)
                        (alist-delete machine r string=?))))
        '()
        (map (cute string-tokenize <> (char-set-complement (char-set #\/)))
             names)))

;;;
;;; Plots.
;;;

(define-record-type plot-type
  (make-plot-type title x-label y-label compute coord)
  plot-type?
  (title    plot-type-title)
  (x-label  plot-type-x-label)
  (y-label  plot-type-y-label)
  (compute  plot-type-compute)
  (coord    plot-type-coordinates))

(define %platform-random-state
  ;; The PRNG state used when generating platforms.  We always use the same
  ;; seed so that results are reproducible.
  (seed->random-state 77))

(define (duration/machines-plot dag algorithms)
  "Execute DAG on several platforms, for each one of ALGORITHMS, and return a
list of per-platform lists of simulation results."
  (define platforms
    ;; Homogeneous clusters of dual-core machines.
    (phase "generating platforms"
           (let ((extra-args `((1 . 1) (1e9 . 1e9)
                               (1e6 . 1e6) (1e-3 . 1e-3)
                               ,%platform-random-state)))
             (unfold (cut >= <> 128)
                     (lambda (m)
                       (apply make-sxml-platform m extra-args))
                     (cut * 2 <>)
                     1))))

  (map (lambda (p)
         (map (lambda (algo)
                `((algorithm . ,(car algo))
                  ,@(simulate dag p (cdr algo))))
              algorithms))
       platforms))

(define duration/machines
  (make-plot-type "Scalability"
                  "number of cores" "schedule length"
                  duration/machines-plot
                  (lambda (result)
                    (list (length (assoc-ref result 'workstations))
                          (assoc-ref result 'total-duration)))))


(define (speedup/machines-plot dag algorithms)
  "Execute DAG on several platforms, for each one of ALGORITHMS, and return a
list of per-platform lists of simulation results."
  (define cores/cpu-log2 2)                       ; 4 cores per CPU

  (define platforms
    ;; Homogeneous clusters of dual-core machines.
    (phase "generating platforms"
           (let ((extra-args `(,(cons cores/cpu-log2 cores/cpu-log2)
                               ,(cons %default-cpu-power %default-cpu-power)
                               ,(cons %default-bandwidth %default-bandwidth)
                               (1e-3 . 1e-3)
                               ,%platform-random-state)))
             (unfold (cut > <> 32)
                     (lambda (m)
                       (apply make-sxml-platform m extra-args))
                     (cut + 3 <>)                 ; high resolution!
                     1))))

  (define per-platform
    (map (lambda (p)
           (map (lambda (algo)
                  `((algorithm . ,(car algo))
                    ,@(simulate dag p (cdr algo))))
                algorithms))
         platforms))

  ;; Add fake simulation results corresponding to the optimal scheduling.
  (map (lambda (p)
         (let ((cpmin (assoc-ref (first p) 'minimum-critical-path-length))
               (seq   (assoc-ref (first p)
                                 'minimum-sequential-computation-time))
               (ws    (assoc-ref (first p) 'workstations))
               (wp    (assoc-ref (first p) 'workstation-power)))
           `(((algorithm . optimal-sequential)
              (total-duration . ,(max (/ seq (length ws)) cpmin))
              (minimum-sequential-computation-time . ,seq)
              (workstations . ,ws)
              (workstation-power . ,wp))

             ((algorithm . optimal-parallel)
              (total-duration . ,(max (/ seq (length ws))
                                      (/ cpmin (expt 2 cores/cpu-log2))))
              (minimum-sequential-computation-time . ,seq)
              (workstations . ,ws)
              (workstation-power . ,wp))

             ,@p)))
       per-platform))

(define speedup/machines
  (make-plot-type (lambda (results)
                    (match results
                      ;; Choose results from the second platform since the
                      ;; first one has no network, so no info about the
                      ;; bandwidth; choose the third algorithm, which has
                      ;; real `workstation-power' data.
                      ((_ (_ _ result _ ...) _ ...)
                       (let* ((workstations (assoc-ref result 'workstations))
                              (cores/cpu
                               (average
                                (map length
                                     (map cdr
                                          (workstation-names->machines+cores
                                           workstations))))))
                        (format #f
                                "Scalability (~a cores per CPU; CPU power: ~2,1f; bandwidth: ~a)"
                                cores/cpu
                                (average (pk 'wp (assoc-ref result 'workstation-power)))
                                (let ((bw (assoc-ref result 'bandwidth)))
                                  (if (or (not bw) (null? bw))
                                      "n/a"
                                      (format #f "~2,1f" (average bw)))))))))
                  "number of CPUs" "speedup"
                  speedup/machines-plot
                  (lambda (result)
                    (let* ((workstations (assoc-ref result 'workstations))
                           (cpu-count    (length
                                          (workstation-names->machines+cores
                                           workstations))))
                      (list cpu-count
                            (/ (assoc-ref result
                                          'minimum-sequential-computation-time)
                               (assoc-ref result 'total-duration)))))))

(define %default-cpu-power 1e9)

(define (duration/bandwidth-plot dag algorithms)
  "Execute DAG on several platforms, for each one of ALGORITHMS, and return a
list of per-platform lists of simulation results."

  (define bandwidth
    (unfold (cute > <> (* 10 %default-cpu-power))
            identity
            (cut * <> 10)
            (/ %default-cpu-power 1e6)))

  (define platforms
    (phase "generating platforms"
           (map (lambda (b)
                  (make-sxml-platform 10 '(1 . 1)
                                      (cons %default-cpu-power
                                            %default-cpu-power)
                                      (cons b b) '(1e-3 . 1e-3)
                                      %platform-random-state))
                bandwidth)))

  (map (lambda (p b)
         (map (lambda (algo)
                `((algorithm . ,(car algo))
                  ,@(simulate dag p (cdr algo))))
              algorithms))
       platforms
       bandwidth))

(define duration/bandwidth
  (make-plot-type "Varying Bandwidth"
                  "bandwidth/CPU power" "schedule length"
                  duration/bandwidth-plot
                  (lambda (result)
                    (list (/ (average (assoc-ref result 'bandwidth))
                             %default-cpu-power)
                          (assoc-ref result 'total-duration)))))


(define speedup/bandwidth
  (make-plot-type (format #f "Varying Bandwidth (CPU power: ~2,1f)"
                          %default-cpu-power)
                  "bandwidth/CPU power" "speedup"
                  duration/bandwidth-plot
                  (lambda (result)
                    (list (/ (average (assoc-ref result 'bandwidth))
                             %default-cpu-power)
                          (/ (assoc-ref result
                                        'minimum-sequential-computation-time)
                             (assoc-ref result 'total-duration))))))

(define %default-bandwidth 1e6)

(define (transfer-time/cpu-power-plot dag algorithms)
  "Execute DAG on several platforms, for each one of ALGORITHMS, and return a
list of per-platform lists of simulation results."

  (define cpu-power
    (unfold (cute > <> (* 1e5 %default-bandwidth))
            identity
            (cut * <> 10)
            (* 1e-1 %default-bandwidth)))

  (define platforms
    (phase "generating platforms"
           (map (lambda (p)
                  (make-sxml-platform 10 '(1 . 1) (cons p p)
                                      (cons %default-bandwidth
                                            %default-bandwidth)
                                      '(1e-3 . 1e-3)
                                      %platform-random-state))
                cpu-power)))

  (map (lambda (p c)
         (map (lambda (algo)
                `((algorithm . ,(car algo))
                  (cpu-power . ,c)
                  ,@(simulate dag p (cdr algo))))
              algorithms))
       platforms
       cpu-power))

(define transfer-time/cpu-power
  (make-plot-type (lambda (results)
                    (format #f "Varying CPU Power (~a cores; bandwidth: ~2,1f)"
                            (length (assoc-ref (first (first results))
                                               'workstations))
                            (average (assoc-ref (first (first results))
                                                'bandwidth))))
                  "CPU power" "cumulated transfer time"
                  transfer-time/cpu-power-plot
                  (lambda (result)
                    (list (assoc-ref result 'cpu-power)
                          (assoc-ref result 'total-transfer-time)))))

(define data-transferred/cpu-power
  (make-plot-type (lambda (results)
                    (format #f "Varying CPU Power (bandwidth: ~2,1f)"
                            (average (assoc-ref (first (first results))
                                                'bandwidth))))
                  "CPU power" "amount of data transferred (bytes)"
                  transfer-time/cpu-power-plot
                  (lambda (result)
                    (list (assoc-ref result 'cpu-power)
                          (assoc-ref result 'data-transferred)))))

(define transfer-slowdown/cpu-power
  (make-plot-type (lambda (results)
                    (format #f "Varying CPU Power (bandwidth: ~2,1f)"
                            (average (assoc-ref (first (first results))
                                                'bandwidth))))
                  "CPU power" "average data transfer stretch"
                  transfer-time/cpu-power-plot
                  (lambda (result)
                    (list (assoc-ref result 'cpu-power)
                          (average (assoc-ref result 'transfer-slowdowns))))))

(define speedup/cpu-power
  (make-plot-type (lambda (results)
                    (format #f "Varying CPU Power (bandwidth: ~2,1f)"
                            (average (assoc-ref (first (first results))
                                                'bandwidth))))
                  "CPU power" "speedup"
                  transfer-time/cpu-power-plot
                  (lambda (result)
                    (list (assoc-ref result 'cpu-power)
                          (/ (assoc-ref result
                                        'minimum-sequential-computation-time)
                             (assoc-ref result 'total-duration))))))



(define (speedup/bandwidth-stddev-plot dag algorithms)
  "Execute DAG on several platforms, for each one of ALGORITHMS, and return a
list of per-platform lists of simulation results."
  (define platform-count 10)

  (define average-bandwidth 1e4)

  (define platforms
    ;; Homogeneous cluster of dual-core machines, with heterogeneous network.
    (phase "generating platforms"
           (unfold (cut >= <> platform-count)
                   (lambda (i)
                     (define min
                       (* average-bandwidth (expt 2 (- i))))
                     (define max
                       (* average-bandwidth (expt 2 i)))

                     (make-sxml-platform 8 '(1 . 1) '(1e9 . 1e9)
                                         (cons min max)
                                         '(1e-2 . 1e-2)
                                         %platform-random-state))
                   1+
                   0)))

  (define per-platform
    (map (lambda (p)
           (map (lambda (algo)
                  `((algorithm . ,(car algo))
                    ,@(simulate dag p (cdr algo))))
                algorithms))
         platforms))

  ;; Add fake simulation results corresponding to the optimal scheduling.
  (map (lambda (p)
         (let ((cpmin (assoc-ref (first p) 'minimum-critical-path-length))
               (seq   (assoc-ref (first p)
                                 'minimum-sequential-computation-time))
               (ws    (assoc-ref (first p) 'workstations)))
           `(((algorithm . optimal)
              (total-duration . ,(max (/ seq (length ws)) cpmin))
              (minimum-sequential-computation-time . ,seq)
              (workstations . ,ws)
              (bandwidth    . ,(list average-bandwidth)))
             ,@p)))
       per-platform))

(define speedup/bandwidth-stddev
  (make-plot-type (lambda (results)
                    (format #f "Scalability vs. Heterogeneity (~a cores; average bandwidth: ~2,1f)"
                            (length (assoc-ref (second (first results))
                                               'workstations))
                            (average (assoc-ref (second (first results))
                                                'bandwidth))))
                  "stddev(bandwidth)" "speedup"
                  speedup/bandwidth-stddev-plot
                  (lambda (result)
                    (list (standard-deviation (assoc-ref result 'bandwidth))
                          (/ (assoc-ref result
                                        'minimum-sequential-computation-time)
                             (assoc-ref result 'total-duration))))))

(define (speedup/cpu-stddev-plot dag algorithms)
  "Execute DAG on several platforms, for each one of ALGORITHMS, and return a
list of per-platform lists of simulation results."
  (define platform-count 10)

  (define max-ratio 5.0)
  (define average-cpu-power %default-cpu-power)

  (define platforms
    ;; Heterogeneous cluster of dual-core machines, with homogeneous network.
    (phase "generating platforms"
           (unfold (cut >= <> platform-count)
                   (lambda (i)
                     (define ratio
                       (+ 1.0 (* (/ i platform-count) (1- max-ratio))))
                     (define step
                       (/ (- (* average-cpu-power ratio) average-cpu-power)
                          (+ 1.0 ratio)))
                     (define min
                       (- average-cpu-power step))
                     (define max
                       (+ average-cpu-power step))

                     (make-sxml-platform 6 '(1 . 1) (cons min max)
                                         (cons %default-bandwidth
                                               %default-bandwidth)
                                         '(1e-2 . 1e-2)
                                         %platform-random-state))
                   1+
                   0)))

  (define per-platform
    (map (lambda (p)
           (map (lambda (algo)
                  `((algorithm . ,(car algo))
                    ,@(simulate dag p (cdr algo))))
                algorithms))
         platforms))

  ;; Add fake simulation results corresponding to the optimal scheduling.
  (map (lambda (p)
         (let ((cpmin (assoc-ref (first p) 'minimum-critical-path-length))
               (seq   (assoc-ref (first p)
                                 'minimum-sequential-computation-time))
               (ws    (assoc-ref (first p) 'workstations))
               (cpus  (assoc-ref (first p) 'workstation-power)))
           `(((algorithm . optimal)
              (total-duration . ,(max (/ seq (length ws)) cpmin))
              (minimum-sequential-computation-time . ,seq)
              (workstations . ,ws)
              (workstation-power . ,cpus))
             ,@p)))
       per-platform))

(define speedup/cpu-stddev
  (make-plot-type (lambda (results)
                    (format #f "Scalability vs. Heterogeneity (~a cores; average CPU power: ~2,1f; bandwidth: ~2,1f)"
                            (length (assoc-ref (second (first results))
                                               'workstations))
                            (average (assoc-ref (second (first results))
                                                'workstation-power))
                            (average (assoc-ref (second (first results))
                                                'bandwidth))))
                  "stddev(CPU power)" "speedup"
                  speedup/cpu-stddev-plot
                  (lambda (result)
                    (list (standard-deviation (assoc-ref result 'workstation-power))
                          (/ (assoc-ref result
                                        'minimum-sequential-computation-time)
                             (assoc-ref result 'total-duration))))))


;;;
;;; Command-line entry point.
;;;

(define %defaults
  '((output-type . gnuplot)))

(define %plot-types
  `(("machines"       . ,duration/machines)
    ("speedup"        . ,speedup/machines)
    ("bandwidth"      . ,duration/bandwidth)
    ("bandwidth-speedup" . ,speedup/bandwidth)
    ("transfer-time"  . ,transfer-time/cpu-power)
    ("data-transferred" . ,data-transferred/cpu-power)
    ("transfer-slowdown" . ,transfer-slowdown/cpu-power)
    ("heterogeneous-bandwidth" . ,speedup/bandwidth-stddev)
    ("heterogeneous-cpu"       . ,speedup/cpu-stddev)
    ("speedup/cpu-power" . ,speedup/cpu-power)))

(define (make-plotter data-sets->plot)
  (lambda (plot results port)
    (let ((data-sets
           (simulation-result->data-sets (map (cut assoc-ref <> 'algorithm)
                                              (first results))
                                         results
                                         (plot-type-coordinates
                                          plot)))
          (title (plot-type-title plot)))
      (data-sets->plot (if (procedure? title)
                           (title results)
                           title)
                       (plot-type-x-label plot)
                       (plot-type-y-label plot)
                       data-sets
                       port))))

(define %output-types
  `((raw       . ,(lambda (plot results port)
                    (pretty-print results port)))
    (gnuplot   . ,(make-plotter data-sets->gnuplot))
    (plotutils . ,(make-plotter data-sets->plotutils))))

(define %options
    ;; Command-line options.
  (let ((display-and-exit-proc (lambda (fmt . args)
                                 (lambda _
                                   (apply format #t (string-append fmt "~%")
                                          args)
                                   (exit 0)))))
    (list (option '(#\V "version") #f #f
                  (display-and-exit-proc "hubble 0.0"))
          (option '(#\? "help") #f #f
                  (display-and-exit-proc
                   "Usage: plot [OPTIONS] KIND DAG
Plot the result of a series of simulations of DAG.

  -T, --output-type=TYPE
                        write the output in format TYPE (default: ~A;
                        possible values: ~A)
  -d, --data-sets=FILE  instead of running a series of simulation, use the
                        data sets from file (the output of a `plot' run with
                        `-T raw')

  -?, --help            give this help message
  -V, --version         print program version

DAG is an XML file containing a Hubble DAG of build tasks.

KIND specifies the type of plot to be produced.  Possible values of KIND are:

~A
Report bugs to <ludovic.courtes@inria.fr>.~%"
                   (and=> (assq 'output-type %defaults) cdr)
                   (string-join (map symbol->string
                                     (map car %output-types))
                                ", ")
                   (string-concatenate (map (lambda (t)
                                              (format #f "  - `~A': ~A~%"
                                                      (car t)
                                                      (plot-type-title (cdr t))))
                                            %plot-types))))

          (option '(#\T "output-type") #t #f
                  (lambda (opt name arg result)
                    (alist-cons 'output-type
                                (or (and=> (assq (string->symbol arg)
                                                 %output-types)
                                           car)
                                    (begin
                                      (format (current-error-port)
                                              "~A: unknown output type" arg)
                                      (exit 1)))
                                result)))
          (option '(#\d "data-sets") #t #f
                  (lambda (opt name arg result)
                    (alist-cons 'input-file arg result))))))

(define (plot . args)
  "Load the Nixpkgs DAG and simulate it on a bunch of synthetic platforms
 (from small to large) and with various scheduling algorithms (simple, HEFT,
etc.).  Emit data sets suitable as input to GNU Plotutils's `graph' program."

  (define opts
    (begin
      (setlocale LC_ALL "")
      (args-fold args %options
                 (lambda (opt name arg result)
                   (format (current-error-port) "~A: invalid option"
                           name)
                   (exit 1))
                 (lambda (arg result)
                   (if (assoc-ref result 'plot-type)
                       (if (assoc-ref result 'dag)
                           (begin
                             (format (current-error-port)
                                     "~A: extraneous argument" args)
                             (exit 1))
                           (alist-cons 'dag arg result))
                       (alist-cons 'plot-type
                                   (or (and=> (assoc arg %plot-types) cdr)
                                       (begin
                                         (format (current-error-port)
                                                 "~A: unknown plot type~%"
                                                 arg)
                                         (exit 1)))
                                   result)))
                 %defaults)))

  (define plot
    (or (assoc-ref opts 'plot-type)
        (begin
          (format (current-error-port) "Please specify the plot type.~%")
          (exit 1))))

  (define compute-plot
    (plot-type-compute plot))

  (define data-sets->plot
    (assq-ref %output-types (assq-ref opts 'output-type)))

  (define result
    (or (and=> (assoc-ref opts 'input-file)
               (lambda (file)
                 (with-input-from-file file read)))
        (let ((dag (if (assoc-ref opts 'dag)
                       (load-dag (assoc-ref opts 'dag))
                       (begin
                         (format (current-error-port)
                                 "Please specify a DAG file.~%")
                         (exit 1))))
              (algorithms
               `((simple . ,(lambda (dag ws)
                              (list select-first-task
                                    select-first-workstation)))
                 (random . ,(lambda (dag ws)
                              (list select-random-task
                                    select-random-workstation)))
                 (heft   . ,(lambda (dag ws)
                              (list (make-heft-task-selector dag ws)
                                    select-workstation/heft)))
                 ;; (heft+random   . ,(lambda (dag ws)
                 ;;                     (list (make-heft-task-selector dag ws)
                 ;;                           select-random-workstation)))
                 (simple+many-cores
                  . ,(lambda (dag ws)
                       (list select-first-task
                             select-many-cores)))
                 (heft+many-cores
                  . ,(lambda (dag ws)
                       (list (make-heft-task-selector dag ws)
                             select-many-cores)))
                 (m-heft . ,(lambda (dag ws)
                              (list (make-heft-task-selector dag ws)
                                    select-workstation/m-heft))))))
          (compute-plot dag algorithms))))

  (data-sets->plot plot result (current-output-port))

  #t)
