;;; Hubble --- A simulator of the Hydra/Nix build tools.
;;; Copyright (C) 2010 INRIA
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, version 3 of the License.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;

;;;
;;; Written by Ludovic Courtès <ludovic.courtes@inria.fr>.
;;;

(define-module (test-lib)
  #:use-module (hubble dag)
  #:export (simple-dag))


(define-syntax simple-dag
  (syntax-rules (sources sinks nodes deps refs)
    "Return the simple DAG specified by the given sources, sinks, and nodes."
    ((_
      (sources src ...)
      (sinks   snk ...)
      (nodes
       (n (deps d ...) (refs r ...)) ...))
     (letrec ((n (delay (make-node (symbol->string 'n) #f
                                   (delay (map force (list d ...)))
                                   (delay (map force (list r ...)))
                                   (delay #f) ;; run-time deps
                                   #f #f #f)))
              ...)
       (make-dag (map force (list n ...))
                 (map force (list src ...))
                 (map force (list snk ...)))))))

